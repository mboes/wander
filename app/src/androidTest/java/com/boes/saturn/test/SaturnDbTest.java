package com.boes.saturn.test;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.boes.saturn.data.SaturnContract.PersonEntry;
import com.boes.saturn.data.SaturnContract.PlaceEntry;
import com.boes.saturn.data.SaturnContract.SubEntry;
import com.boes.saturn.data.SaturnDbHelper;

import java.util.Map;
import java.util.Set;

import static com.boes.saturn.data.SaturnDbHelper.DATABASE_NAME;

public class SaturnDbTest extends AndroidTestCase {

    private static final String TAG = SaturnDbTest.class.getSimpleName();

    public void testCreateDb() {
        mContext.deleteDatabase(DATABASE_NAME);
        SaturnDbHelper helper = new SaturnDbHelper(mContext);
        SQLiteDatabase db = helper.getWritableDatabase();
        assertTrue("Db closed", db.isOpen());
        helper.close();
    }

    public void testInsertAndReadDb() {
        SaturnDbHelper helper = new SaturnDbHelper(mContext);
        SQLiteDatabase db = helper.getWritableDatabase();

        ContentValues matt = getMockPerson("boes.matt");
        long personId = db.insert(PersonEntry.TABLE_NAME, null, matt);
        assertTrue("Insert failed", personId != -1);
        Log.d(TAG, "Person id = " + personId);
        matt.put(PersonEntry._ID, personId);

        String[] personCols = { PersonEntry._ID, PersonEntry.REMOTE_KEY, PersonEntry.NAME };
        Cursor personCursor = db.query(PersonEntry.TABLE_NAME, personCols, null, null, null, null, null);
        assertTrue("Cursor is empty", personCursor.moveToFirst());
        validateCursor(matt, personCursor);

        ContentValues stanford = getMockPlace("Stanford University", personId, "Go Cardinal!", 37.42991957045861, -122.16942820698023, 0);
        long placeId = db.insert(PlaceEntry.TABLE_NAME, null, stanford);
        assertTrue("Insert failed", placeId != -1);
        Log.d(TAG, "Place id = " + placeId);
        stanford.put(PlaceEntry._ID, placeId);

        Cursor placeCursor = db.query(PlaceEntry.TABLE_NAME, null, null, null, null, null, null);
        assertTrue("Cursor is empty", placeCursor.moveToFirst());
        validateCursor(stanford, placeCursor);

        ContentValues rodin = getMockPlace("Rodin Garden", personId, "Large outdoor collection", 37.43236309856673, -122.1709620952606, 0);
        rodin.put(PlaceEntry._ID, db.insert(PlaceEntry.TABLE_NAME, null, rodin));

        ContentValues relation = getMockSub(rodin, stanford, 0);
        long relationId = db.insert(SubEntry.TABLE_NAME, null, relation);
        assertTrue("Insert failed", relationId != -1);
        Log.d(TAG, "Relation id = " + relationId);
        relation.put(SubEntry._ID, relationId);

        Cursor subCursor = db.query(SubEntry.TABLE_NAME, null, null, null, null, null, null);
        assertTrue("Cursor is empty", subCursor.moveToFirst());
        validateCursor(relation, subCursor);

        stanford.put(PlaceEntry.SUB_COUNT, 1);
        String[] selectionArgs = { stanford.getAsString(PlaceEntry._ID) };

        int nUpdated = db.update(PlaceEntry.TABLE_NAME, stanford, PlaceEntry._ID + " = ?", selectionArgs);
        assertTrue("Wrong number updated", nUpdated == 1);

        Cursor placeCursor2 = db.query(PlaceEntry.TABLE_NAME, null, PlaceEntry._ID + " = ?", selectionArgs, null, null, null);
        assertTrue("Cursor is empty", placeCursor2.moveToFirst());
        assertEquals("Wrong count", 1, placeCursor2.getCount());
        validateCursor(stanford, placeCursor2);

        helper.close();
    }

    private void validateCursor(ContentValues expected, Cursor cursor) {
        Set<Map.Entry<String, Object>> keyToValueSet = expected.valueSet();

        for (Map.Entry<String, Object> entry : keyToValueSet) {
            String key = entry.getKey();
            int column = cursor.getColumnIndex(key);
            assertTrue("Column " + key + " does not exist", column != -1);

            if (PlaceEntry.LATITUDE.equals(key) || PlaceEntry.LONGITUDE.equals(key)) {
                Double value = (Double) entry.getValue();
                Double actual = cursor.getDouble(column);
                Log.d(TAG, "key = " + key + ", " + "value = " + actual);
                assertEquals("Wrong value", value, actual);
            } else {
                String value = entry.getValue().toString();
                String actual = cursor.getString(column);
                Log.d(TAG, "key = " + key + ", " + "value = " + actual);
                assertEquals("Wrong value", value, actual);
            }
        }

        cursor.close();
    }

    private ContentValues getMockPerson(String name) {
        ContentValues values = new ContentValues();
        values.put(PersonEntry.NAME, name);
        return values;
    }

    private ContentValues getMockPlace(String name, long ownerId, String description, double latitude, double longitude, int subCount) {
        ContentValues values = new ContentValues();
        values.put(PlaceEntry.NAME, name);
        values.put(PlaceEntry.OWNER_ID, ownerId);
        values.put(PlaceEntry.DESCRIPTION, description);
        values.put(PlaceEntry.LATITUDE, latitude);
        values.put(PlaceEntry.LONGITUDE, longitude);
        values.put(PlaceEntry.SUB_COUNT, subCount);
        return values;
    }

    private ContentValues getMockSub(ContentValues sub, ContentValues parent, int position) {
        long subId = sub.getAsLong(PlaceEntry._ID);
        long parentId = parent.getAsLong(PlaceEntry._ID);

        ContentValues values = new ContentValues();
        values.put(SubEntry.SUB_ID, subId);
        values.put(SubEntry.PARENT_ID, parentId);
        values.put(SubEntry.POSITION, position);
        return values;
    }

}
