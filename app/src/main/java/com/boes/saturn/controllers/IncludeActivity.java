package com.boes.saturn.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.endpoint.App;
import com.boes.saturn.interfaces.Constant;
import com.boes.saturn.interfaces.Endpoint;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;

import java.io.IOException;
import java.util.Arrays;

import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;

public class IncludeActivity extends TabActivity {

    private static final String TAG = IncludeActivity.class.getSimpleName();

    private String mParentKey;
    private String mSubKey;
    private Subscription mSubscription;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        showActionBarTitle("Include in " + getParentName(getIntent()));

        if (savedInstanceState == null || !savedInstanceState.containsKey(TAB_INDEX)) {
            setDefaultTab(2);
        }
    }

    @Override
    public void onPlacePicked(PlaceBean bean) {
        mParentKey = getParentKey(getIntent());
        mSubKey = bean.getWebSafeKey();

        onRequest();
        mSubscription = getSubscription();
    }

    private String getParentKey(Intent i) {
        return i.getStringExtra(Constant.WEB_SAFE_KEY);
    }

    private String getParentName(Intent i) {
        return i.getStringExtra(Constant.PLACE_NAME);
    }

    private Subscription getSubscription() {
        PlaceBean parent = new PlaceBean();
        parent.setWebSafeKey(mParentKey);
        parent.setSubKeysToAdd(Arrays.asList(mSubKey));

        App app = (App) getApplication();
        Endpoint endpoint = app.getEndpoint();
        return AndroidObservable.bindActivity(this, endpoint.savePlace(parent)).subscribe(new SaveObserver());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    private class SaveObserver implements Observer<PlaceBean> {

        private ProgressBar mProgressBar;

        public SaveObserver() {
            mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        }

        @Override
        public void onNext(PlaceBean bean) {
            IncludeActivity.this.setResult(Activity.RESULT_OK);
        }

        @Override
        public void onCompleted() {
            mProgressBar.setVisibility(View.INVISIBLE);
            IncludeActivity.this.finish();
        }

        @Override
        public void onError(Throwable e) {
            mSubscription.unsubscribe();
            mProgressBar.setVisibility(View.INVISIBLE);

            Log.e(IncludeActivity.TAG, e.toString());

            if (e instanceof GoogleJsonResponseException) {
                GoogleJsonResponseException je = (GoogleJsonResponseException) e;
                String message = je.getDetails().getMessage();
                Toast.makeText(IncludeActivity.this, message, Toast.LENGTH_SHORT).show();
            } else if (e instanceof IOException) {
                Toast.makeText(IncludeActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        }

    }

}
