package com.boes.saturn.controllers;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.data.SerializedPlace;
import com.boes.saturn.endpoint.App;
import com.boes.saturn.interfaces.Constant;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;

public class HomeActivity extends TabActivity {

    private static final int REQUEST_ACCOUNT = 301;

    @Override
    public void onPlacePicked(PlaceBean bean) {
        Intent i = new Intent(this, ViewActivity.class);
        i.putExtra(Constant.PLACE_BEAN, new SerializedPlace(bean));
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_add) {
            Intent i = new Intent(this, EditActivity.class);
            startActivity(i);
            return true;
        }

        if (id == R.id.action_feedback) {
            Intent i = new Intent(Intent.ACTION_SENDTO);
            i.setData(Uri.parse("mailto:"));
            i.putExtra(Intent.EXTRA_EMAIL, new String[] { "boes.matt@gmail.com" });
            i.putExtra(Intent.EXTRA_SUBJECT, "Wander Feedback");
            if (i.resolveActivity(getPackageManager()) != null) {
                startActivity(i);
            }
            return true;
        }

        if (id == R.id.action_profile) {
            App app = (App) getApplication();
            String name = app.getUserAccount();
            Intent i = AccountPicker.newChooseAccountIntent(getAccount(name), null, new String[]{ GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE }, true, null, null, null, null);
            startActivityForResult(i, REQUEST_ACCOUNT);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Account getAccount(String name) {
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
        for (Account account : accounts) {
            if (account.name.equals(name)) {
                return account;
            }
        }

        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ACCOUNT) {
            if (resultCode == Activity.RESULT_OK) {
                String account = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                App app = (App) getApplication();
                app.setUserAccount(account);

                FragmentManager fm = getFragmentManager();
                Fragment fragment = fm.findFragmentById(R.id.content);
                fm.beginTransaction().detach(fragment).commit();
                fm.beginTransaction().attach(fragment).commit();
            }
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

}
