package com.boes.saturn.controllers;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.data.SerializedPlace;
import com.boes.saturn.interfaces.Constant;
import com.boes.saturn.interfaces.Endpoint;
import com.boes.saturn.interfaces.PlaceListener;

import java.io.IOException;

import rx.Subscription;
import rx.android.observables.AndroidObservable;

public class PersonActivity extends GooglePlayServicesActivity implements PlaceListener {

    private ProgressBar mProgressBar;
    private TextView mStatusMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_places);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mStatusMessage = (TextView) findViewById(R.id.status_message);

        String name = getIntent().getStringExtra(Constant.PERSON_NAME);
        setTitle(name);
        String webSafeKey = getIntent().getStringExtra(Constant.WEB_SAFE_KEY);

        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentByTag("person");
        if (fragment == null) {
            fm.beginTransaction()
              .add(R.id.content, PersonFragment.newInstance(webSafeKey), "person")
              .commit();
        }
    }

    @Override
    public void onPlacePicked(PlaceBean bean) {
        Intent i = new Intent(this, ViewActivity.class);
        i.putExtra(Constant.PLACE_BEAN, new SerializedPlace(bean));
        startActivity(i);
    }

    @Override
    public void onRequest() {
        mStatusMessage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCompleted() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError(String tag, Throwable e, final Runnable callback) {
        mProgressBar.setVisibility(View.INVISIBLE);

        Log.e(tag, e.toString());

        if (e instanceof IOException) {
            Toast.makeText(this, "Network error", Toast.LENGTH_SHORT).show();
        }

        mStatusMessage.setText("Retry");
        mStatusMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStatusMessage.setVisibility(View.INVISIBLE);
                mProgressBar.setVisibility(View.VISIBLE);
                callback.run();
            }
        });
        mStatusMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void setStatusMessage(String message) {
        mStatusMessage.setText(message);
        mStatusMessage.setVisibility(View.VISIBLE);
    }

    public static class PersonFragment extends PlaceListFragment {

        public static PersonFragment newInstance(String webSafeKey) {
            PersonFragment fragment = new PersonFragment();
            Bundle args = new Bundle();
            args.putString(Constant.WEB_SAFE_KEY, webSafeKey);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public Subscription getSubscription() {
            Endpoint endpoint = getEndpoint();
            String webSafeKey = getArguments().getString(Constant.WEB_SAFE_KEY);
            return AndroidObservable.bindFragment(this, endpoint.getPlacesByPerson(webSafeKey)).subscribe(this);
        }

    }

}
