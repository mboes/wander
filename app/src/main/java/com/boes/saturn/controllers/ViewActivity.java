package com.boes.saturn.controllers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PersonBean;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.data.SerializedPlace;
import com.boes.saturn.endpoint.App;
import com.boes.saturn.interfaces.Constant;
import com.boes.saturn.interfaces.Endpoint;
import com.boes.saturn.views.ViewUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;

public class ViewActivity extends GooglePlayServicesActivity implements
        Observer<List<PlaceBean>>,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener,
        ViewPager.OnPageChangeListener {

    private static final String TAG = ViewActivity.class.getSimpleName();
    private static final String PAGE_INDEX = "page_index";
    private static final int REQUEST_EDIT_PLACE = 201;
    private static final int REQUEST_INCLUDE_PLACE = 202;

    private PlaceBean mPlaceBean;
    private Subscription mSubscription;

    private GoogleMap mMap;
    private List<Marker> mMarkers;
    private IconGenerator mIconFactory;

    private ViewPager mPager;
    private PlaceBeanPagerAdapter mPagerAdapter;
    private int mSavedPageIndex;

    private ProgressBar mProgressBar;
    private TextView mStatusMessage;

    private Subscription mDeleteSubscription;
    private Subscription mRemoveSubscription;
    private boolean isMarkerDragComplete;
    private boolean isMarkerBeingRemoved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        ActionBar bar = getActionBar();
        assert bar != null;

        mPlaceBean = getPlaceBean(getIntent());
        setTitle(mPlaceBean.getName());

        FragmentManager fm = getFragmentManager();
        MapFragment fragment = (MapFragment) fm.findFragmentById(R.id.map);
        mMap = fragment.getMap();
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnMarkerDragListener(this);

        LatLng center = new LatLng(mPlaceBean.getLatitude(), mPlaceBean.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 17));

        mMarkers = new ArrayList<Marker>();
        mIconFactory = new IconGenerator(this);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.pager_page_margin));
        mPager.setOnPageChangeListener(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mStatusMessage = (TextView) findViewById(R.id.status_message);

        if (savedInstanceState != null) {
            mSavedPageIndex = savedInstanceState.getInt(PAGE_INDEX, 0);
        }
    }

    private PlaceBean getPlaceBean(Intent i) {
        SerializedPlace place = (SerializedPlace) i.getSerializableExtra(Constant.PLACE_BEAN);
        return place.getBean();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult called with requestCode = " + requestCode + " and resultCode = " + resultCode);

        if (requestCode == REQUEST_EDIT_PLACE) {
            if (resultCode == Activity.RESULT_OK) {
                mPlaceBean = getPlaceBean(data);
                setTitle(mPlaceBean.getName());
                invalidateOptionsMenu();
            }
            return;
        }

        if (requestCode == REQUEST_INCLUDE_PLACE) {
            if (resultCode == Activity.RESULT_OK) {
                int subCount = mPlaceBean.getSubCount();
                subCount++;
                mPlaceBean.setSubCount(subCount);
                mSavedPageIndex = mMarkers.size();  // move to included place

                if (subCount == 1) {
                    Toast.makeText(this, "Press and hold map marker to remove place", Toast.LENGTH_LONG).show();
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart called");
        super.onStart();

        mStatusMessage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mSubscription = getSubscription();
    }

    private Subscription getSubscription() {
        String webSafeKey = mPlaceBean.getWebSafeKey();
        App app = (App) getApplication();
        Endpoint endpoint = app.getEndpoint();
        return AndroidObservable.bindActivity(this, endpoint.getSubPlaces(webSafeKey)).subscribe(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view, menu);

        App app = (App) getApplication();
        PersonBean owner = mPlaceBean.getOwner();
        if (!app.isUserOwner(owner)) {
            menu.removeItem(R.id.action_edit);
            menu.removeItem(R.id.action_include);
            menu.removeItem(R.id.action_delete);
        } else {
            MenuItem deleteAction = menu.findItem(R.id.action_delete);
            deleteAction.setTitle("Delete " + mPlaceBean.getName());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_edit) {
            Intent i = new Intent(this, EditActivity.class);
            i.putExtra(Constant.PLACE_BEAN, new SerializedPlace(mPlaceBean));
            startActivityForResult(i, REQUEST_EDIT_PLACE);
            return true;
        }

        if (id == R.id.action_include) {
            Intent i = new Intent(this, IncludeActivity.class);
            i.putExtra(Constant.WEB_SAFE_KEY, mPlaceBean.getWebSafeKey());
            i.putExtra(Constant.PLACE_NAME, mPlaceBean.getName());
            startActivityForResult(i, REQUEST_INCLUDE_PLACE);
            return true;
        }

        if (id == R.id.action_delete) {
            String webSafeKey = mPlaceBean.getWebSafeKey();
            App app = (App) getApplication();
            Endpoint endpoint = app.getEndpoint();
            mProgressBar.setVisibility(View.VISIBLE);
            mDeleteSubscription = AndroidObservable.bindActivity(this, endpoint.deletePlace(webSafeKey)).subscribe(new DeleteObserver());
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNext(List<PlaceBean> beans) {
        setPrimaryPlace();

        boolean isRemovable = false;
        App app = (App) getApplication();
        PersonBean owner = mPlaceBean.getOwner();
        if (app.isUserOwner(owner)) {
            isRemovable = true;
        }

        for (PlaceBean bean : beans) {
            addMarker(bean, isRemovable);
            mPagerAdapter.addCard(bean);
        }
        mPagerAdapter.notifyDataSetChanged();

        int index = Math.min(mSavedPageIndex, mMarkers.size() - 1);
        Marker marker = mMarkers.get(index);
        animateMapTo(marker.getPosition());
        mPager.setCurrentItem(index);
    }

    @Override
    public void onCompleted() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError(Throwable e) {
        mSubscription.unsubscribe();
        mProgressBar.setVisibility(View.INVISIBLE);

        Log.e(TAG, e.toString());

        if (e instanceof IOException) {
            Toast.makeText(this, "Network error", Toast.LENGTH_SHORT).show();
        }

        mStatusMessage.setText("Retry");
        mStatusMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStatusMessage.setVisibility(View.INVISIBLE);
                mProgressBar.setVisibility(View.VISIBLE);
                mSubscription = getSubscription();
            }
        });
        mStatusMessage.setVisibility(View.VISIBLE);
    }

    private void setPrimaryPlace() {
        mMap.clear();
        mMarkers.clear();

        mPagerAdapter = new PlaceBeanPagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        addMarker(mPlaceBean, false);
        mPagerAdapter.addCard(mPlaceBean);
    }

    private LatLng addMarker(PlaceBean bean, boolean isRemovable) {
        ViewUtils.setMarkerStyle(mIconFactory, bean);
        Bitmap icon = mIconFactory.makeIcon(bean.getName());
        LatLng latLng = new LatLng(bean.getLatitude(), bean.getLongitude());

        MarkerOptions opts = new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(icon))
                .position(latLng)
                .draggable(isRemovable);

        Marker marker = mMap.addMarker(opts);
        mMarkers.add(marker);
        return latLng;
    }

    private void animateMapTo(LatLng target) {
        mMap.animateCamera(CameraUpdateFactory.newLatLng(target));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        int position = mMarkers.indexOf(marker);
        animateMapTo(marker.getPosition());
        mPager.setCurrentItem(position);
        mSavedPageIndex = position;
        return true;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Log.d(TAG, "onMarkerDragStart");
        isMarkerDragComplete = false;
        isMarkerBeingRemoved = true;
        marker.setVisible(false);

        int position = mMarkers.indexOf(marker);
        String subKey = mPagerAdapter.getPlaceBean(position).getWebSafeKey();

        PlaceBean parent = new PlaceBean();
        parent.setWebSafeKey(mPlaceBean.getWebSafeKey());
        parent.setSubKeysToRemove(Arrays.asList(subKey));

        App app = (App) getApplication();
        Endpoint endpoint = app.getEndpoint();
        mProgressBar.setVisibility(View.VISIBLE);
        mRemoveSubscription = AndroidObservable.bindActivity(this, endpoint.savePlace(parent)).subscribe(new RemoveObserver(marker, position));
    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        if (!isMarkerBeingRemoved) {
            int position = mMarkers.indexOf(marker);
            redisplayMarker(marker, position);
        }
        isMarkerDragComplete = true;
    }

    @Override
    public void onPageSelected(int position) {
        mSavedPageIndex = position;
        Marker marker = mMarkers.get(position);
        animateMapTo(marker.getPosition());
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    protected void onSaveInstanceState(@Nonnull Bundle outState) {
        outState.putInt(PAGE_INDEX, mSavedPageIndex);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSubscription.unsubscribe();

        if (mDeleteSubscription != null) {
            mDeleteSubscription.unsubscribe();
        }

        if (mRemoveSubscription != null) {
            mRemoveSubscription.unsubscribe();
        }
    }

    public static class PlaceBeanPagerAdapter extends FragmentStatePagerAdapter {

        List<PlaceBean> beans;

        public PlaceBeanPagerAdapter(FragmentManager fm) {
            super(fm);
            beans = new ArrayList<PlaceBean>();
        }

        @Override
        public Fragment getItem(int position) {
            PlaceBean bean = beans.get(position);
            return PlaceBeanFragment.newInstance(bean);
        }

        @Override
        public int getCount() {
            return beans.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            PlaceBean bean = beans.get(position);
            return bean.getName();
        }

        public void addCard(PlaceBean bean) {
            beans.add(bean);
        }

        public PlaceBean getPlaceBean(int position) {
            return beans.get(position);
        }

    }

    public static class PlaceBeanFragment extends Fragment {

        public static PlaceBeanFragment newInstance(PlaceBean bean) {
            PlaceBeanFragment fragment = new PlaceBeanFragment();
            Bundle args = new Bundle();
            args.putSerializable(Constant.PLACE_BEAN, new SerializedPlace(bean));
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            Bundle args = getArguments();
            SerializedPlace place = (SerializedPlace) args.getSerializable(Constant.PLACE_BEAN);
            PlaceBean bean = place.getBean();

            View v = ViewUtils.getPlaceView(inflater, R.layout.item_place, container);
            ViewUtils.initPlaceItem(getActivity(), v, bean);

            TextView tvName = (TextView) v.findViewById(R.id.tvName);
            tvName.setVisibility(View.GONE);

            return v;
        }

    }

    private class DeleteObserver implements Observer<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            // always true
        }

        @Override
        public void onCompleted() {
            mProgressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(ViewActivity.this, "Deleted", Toast.LENGTH_SHORT).show();
            ViewActivity.this.finish();
        }

        @Override
        public void onError(Throwable e) {
            mDeleteSubscription.unsubscribe();
            mProgressBar.setVisibility(View.INVISIBLE);

            Log.e(TAG, e.toString());

            if (e instanceof IOException) {
                Toast.makeText(ViewActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private class RemoveObserver implements Observer<PlaceBean> {

        private Marker mMarkerInQuestion;
        private int mMarkerPosition;

        public RemoveObserver(Marker marker, int position) {
            mMarkerInQuestion = marker;
            mMarkerPosition = position;
        }

        @Override
        public void onNext(PlaceBean bean) {

        }

        @Override
        public void onCompleted() {
            if (mSavedPageIndex >= mMarkerPosition) {
                mSavedPageIndex--;
            }
            int subCount = mPlaceBean.getSubCount();
            subCount--;
            mPlaceBean.setSubCount(subCount);

            mSubscription = getSubscription();
            Toast.makeText(ViewActivity.this, "Removed", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onError(Throwable e) {
            mRemoveSubscription.unsubscribe();
            mProgressBar.setVisibility(View.INVISIBLE);
            isMarkerBeingRemoved = false;

            if (isMarkerDragComplete) {
                redisplayMarker(mMarkerInQuestion, mMarkerPosition);
            }

            Log.e(TAG, e.toString());

            if (e instanceof IOException) {
                Toast.makeText(ViewActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void redisplayMarker(Marker marker, int position) {
        PlaceBean bean = mPagerAdapter.getPlaceBean(position);
        LatLng latLng = new LatLng(bean.getLatitude(), bean.getLongitude());
        marker.setPosition(latLng);
        marker.setVisible(true);
    }

}
