package com.boes.saturn.controllers;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.endpoint.App;
import com.boes.saturn.interfaces.Endpoint;
import com.boes.saturn.interfaces.PlaceListener;
import com.boes.saturn.views.PlaceBeanListAdapter;

import java.util.List;

import rx.Observer;
import rx.Subscription;

public abstract class PlaceListFragment extends Fragment implements
        Observer<List<PlaceBean>>,
        AdapterView.OnItemClickListener {

    private static final String TAG = PlaceListFragment.class.getSimpleName();

    private PlaceListener mListener;
    private PlaceBeanListAdapter mAdapter;
    private Subscription mSubscription;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (PlaceListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new PlaceBeanListAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView v = (ListView) inflater.inflate(R.layout.fragment_list, container, false);
        v.setAdapter(mAdapter);
        v.setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mListener.onRequest();
        mSubscription = getSubscription();
    }

    protected Endpoint getEndpoint() {
        Activity activity = getActivity();
        App app = (App) activity.getApplication();
        return app.getEndpoint();
    }

    protected abstract Subscription getSubscription();

    @Override
    public void onNext(List<PlaceBean> beans) {
        mAdapter.clear();
        mAdapter.addAll(beans);
    }

    @Override
    public void onCompleted() {
        mListener.onCompleted();
    }

    @Override
    public void onError(Throwable e) {
        mSubscription.unsubscribe();
        Runnable callback = new Runnable() {
            @Override
            public void run() {
                mSubscription = getSubscription();
            }
        };
        mListener.onError(TAG, e, callback);
    }

    protected void setStatusIfEmpty(String message) {
        if (mAdapter.isEmpty()) {
            mListener.setStatusMessage(message);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PlaceBean bean = mAdapter.getItem(position);
        mListener.onPlacePicked(bean);
    }

    @Override
    public void onStop() {
        super.onStop();
        mSubscription.unsubscribe();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
