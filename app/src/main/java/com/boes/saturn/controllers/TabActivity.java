package com.boes.saturn.controllers;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.endpoint.App;
import com.boes.saturn.interfaces.Endpoint;
import com.boes.saturn.interfaces.PlaceListener;
import com.boes.saturn.views.ViewUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nonnull;

import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;

public abstract class TabActivity extends GooglePlayServicesActivity implements
        TabListener,
        PlaceListener {

    protected static final String TAB_INDEX = "tab_index";
    private static final String WANDER_TAB = "Wander";
    private static final String MAP_TAB = "Map";
    private static final String MY_TAB = "My";

    private ActionBar mActionBar;
    private ProgressBar mProgressBar;
    private TextView mStatusMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mStatusMessage = (TextView) findViewById(R.id.status_message);

        mActionBar = getActionBar();
        assert mActionBar != null;
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        Tab tab1 = mActionBar.newTab();
        tab1.setText(WANDER_TAB);
        tab1.setTabListener(this);
        mActionBar.addTab(tab1, 0);

        Tab tab2 = mActionBar.newTab();
        tab2.setText(MAP_TAB);
        tab2.setTabListener(this);
        mActionBar.addTab(tab2, 1);

        Tab tab3 = mActionBar.newTab();
        tab3.setText(MY_TAB);
        tab3.setTabListener(this);
        mActionBar.addTab(tab3, 2);
    }

    protected void setDefaultTab(int index) {
        mActionBar.setSelectedNavigationItem(index);
    }

    protected void showActionBarTitle(String title) {
        mActionBar.setTitle(title);
        mActionBar.setDisplayShowTitleEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);
    }

    @Override
    protected void onRestoreInstanceState(@Nonnull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int index = savedInstanceState.getInt(TAB_INDEX, 0);
        mActionBar.setSelectedNavigationItem(index);
    }

    @Override
    protected void onSaveInstanceState(@Nonnull Bundle outState) {
        outState.putInt(TAB_INDEX, mActionBar.getSelectedNavigationIndex());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        String tag = tab.getText().toString();
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentByTag(tag);

        if (fragment != null) {
            ft.attach(fragment);
        } else {
            ft.add(R.id.content, instantiateFragment(tag), tag);
        }
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        String tag = tab.getText().toString();
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentByTag(tag);
        ft.detach(fragment);
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {

    }

    private Fragment instantiateFragment(String tag) {
        if (WANDER_TAB.equals(tag)) {
            return new WanderFragment();
        }

        if (MAP_TAB.equals(tag)) {
            return new NearbyFragment();
        }

        if (MY_TAB.equals(tag)) {
            return new MyFragment();
        }

        return new Fragment();
    }

    @Override
    public void onRequest() {
        mStatusMessage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCompleted() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onError(String tag, Throwable e, final Runnable callback) {
        mProgressBar.setVisibility(View.INVISIBLE);

        Log.e(tag, e.toString());

        if (e instanceof IOException) {
            Toast.makeText(this, "Network error", Toast.LENGTH_SHORT).show();
        }

        mStatusMessage.setText("Retry");
        mStatusMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStatusMessage.setVisibility(View.INVISIBLE);
                mProgressBar.setVisibility(View.VISIBLE);
                callback.run();
            }
        });
        mStatusMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void setStatusMessage(String message) {
        mStatusMessage.setText(message);
        mStatusMessage.setVisibility(View.VISIBLE);
    }

    public static class WanderFragment extends PlaceListFragment {

        @Override
        public Subscription getSubscription() {
            Endpoint endpoint = getEndpoint();
            return AndroidObservable.bindFragment(this, endpoint.findPlaces(100)).subscribe(this);
        }

    }

    public static class MyFragment extends PlaceListFragment {

        @Override
        public Subscription getSubscription() {
            Endpoint endpoint = getEndpoint();
            return AndroidObservable.bindFragment(this, endpoint.getPlacesByPerson(null)).subscribe(this);
        }

        @Override
        public void onCompleted() {
            super.onCompleted();
            setStatusIfEmpty("Go ahead, add a place");
        }

    }

    public static class NearbyFragment extends MapFragment implements
            GoogleMap.OnMarkerClickListener,
            Observer<List<PlaceBean>> {

        private static final String TAG = NearbyFragment.class.getSimpleName();
        private static final String LAT_LNG = "latLng";

        private PlaceListener mListener;

        private GoogleMap mMap;
        private Map<Marker, PlaceBean> mMarkersToBeans;
        private boolean isLocationSet;

        private Endpoint mEndpoint;
        private Subscription mSubscription;

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            mListener = (PlaceListener) activity;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mMarkersToBeans = new HashMap<Marker, PlaceBean>();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = super.onCreateView(inflater, container, savedInstanceState);

            mMap = getMap();
            mMap.setMyLocationEnabled(true);
            mMap.setOnMarkerClickListener(this);

            if (savedInstanceState != null && savedInstanceState.containsKey(LAT_LNG)) {
                LatLng latLng = savedInstanceState.getParcelable(LAT_LNG);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                isLocationSet = true;
            }

            return v;
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            if (mMap != null) {
                LatLng latLng = mMap.getCameraPosition().target;
                outState.putParcelable(LAT_LNG, latLng);
            }
            super.onSaveInstanceState(outState);
        }

        @Override
        public void onStart() {
            super.onStart();
            GooglePlayServicesActivity activity = (GooglePlayServicesActivity) getActivity();

            if (!isLocationSet) {
                Location location = activity.getLocation();
                if (location != null) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                    isLocationSet = true;
                }
            }

            App app = (App) activity.getApplication();
            mEndpoint = app.getEndpoint();

            mListener.onRequest();
            mSubscription = getSubscription();
        }

        private Subscription getSubscription() {
            return AndroidObservable.bindFragment(this, mEndpoint.findPlaces(100)).subscribe(this);
        }

        @Override
        public void onNext(List<PlaceBean> beans) {
            mMarkersToBeans.clear();
            mMap.clear();

            IconGenerator factory = new IconGenerator(getActivity());
            for (PlaceBean bean : beans) {
                ViewUtils.setMarkerStyle(factory, bean);
                Bitmap icon = factory.makeIcon(bean.getName());

                MarkerOptions opts = new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(icon))
                        .position(new LatLng(bean.getLatitude(), bean.getLongitude()));

                Marker marker = mMap.addMarker(opts);
                mMarkersToBeans.put(marker, bean);
            }
        }

        @Override
        public void onCompleted() {
            mListener.onCompleted();
        }

        @Override
        public void onError(Throwable e) {
            mSubscription.unsubscribe();
            Runnable callback = new Runnable() {
                @Override
                public void run() {
                    mSubscription = getSubscription();
                }
            };
            mListener.onError(TAG, e, callback);
        }

        @Override
        public boolean onMarkerClick(Marker marker) {
            PlaceBean bean = mMarkersToBeans.get(marker);
            mListener.onPlacePicked(bean);
            return true;
        }

        @Override
        public void onStop() {
            super.onStop();
            mSubscription.unsubscribe();
        }

        @Override
        public void onDetach() {
            super.onDetach();
            mListener = null;
        }

    }

}
