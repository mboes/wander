package com.boes.saturn.controllers;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.data.SerializedPlace;
import com.boes.saturn.endpoint.App;
import com.boes.saturn.interfaces.Constant;
import com.boes.saturn.interfaces.Endpoint;
import com.boes.saturn.views.ViewUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.maps.android.ui.IconGenerator;

import java.io.IOException;

import javax.annotation.Nonnull;

import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;

public class EditActivity extends GooglePlayServicesActivity {

    private static final String TAG = EditActivity.class.getSimpleName();
    private static final String LAT_LNG = "latLng";

    private PlaceBean mPlaceBean;
    private Subscription mSubscription;

    private EditText mNameField;
    private EditText mDescriptionField;

    private GoogleMap mMap;
    // private Marker mPrimaryMarker;
    // private List<Marker> mSubMarkers;

    private LatLng mCenterLatLng;
    private Bitmap mCenterMarkerBitmap;
    private ImageView mCenterMarkerImageView;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        ActionBar bar = getActionBar();
        assert bar != null;

        if (savedInstanceState != null && savedInstanceState.containsKey(LAT_LNG)) {
            mCenterLatLng = savedInstanceState.getParcelable(LAT_LNG);
        }

        mPlaceBean = getPlaceBean(getIntent());

        mNameField = (EditText) findViewById(R.id.name_field);
        mDescriptionField = (EditText) findViewById(R.id.description_field);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mCenterMarkerImageView = (ImageView) findViewById(R.id.center_marker);

        FragmentManager fm = getFragmentManager();
        MapFragment fragment = (MapFragment) fm.findFragmentById(R.id.map);
        mMap = fragment.getMap();
        mMap.setMyLocationEnabled(true);

        mProgressBar.setVisibility(View.INVISIBLE);

        String name = "New Place";
        if (mPlaceBean != null) {
            setTitle("Edit place");

            name = mPlaceBean.getName();
            mNameField.setText(name);
            mDescriptionField.setText(mPlaceBean.getDescription());

            if (mCenterLatLng == null) {
                mCenterLatLng = new LatLng(mPlaceBean.getLatitude(), mPlaceBean.getLongitude());
            }
        }
        IconGenerator iconFactory = new IconGenerator(this);
        ViewUtils.setMarkerStyle(iconFactory, mPlaceBean);
        mCenterMarkerBitmap = iconFactory.makeIcon(name);
        mCenterMarkerImageView.setPadding(0, 0, 0, mCenterMarkerBitmap.getHeight());

        if (mCenterLatLng != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCenterLatLng, 19));
        }
        mCenterMarkerImageView.setImageBitmap(mCenterMarkerBitmap);
    }

    private PlaceBean getPlaceBean(Intent i) {
        SerializedPlace place = (SerializedPlace) i.getSerializableExtra(Constant.PLACE_BEAN);
        return place == null ? null : place.getBean();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);

        if (mCenterLatLng == null) {
            Location location = getLocation();
            if (location != null) {
                mCenterLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCenterLatLng, 19));
            }
        }
    }

    public void onCancel(View v) {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    public void onSave(View v) {
        if (mPlaceBean == null) {
            mPlaceBean = new PlaceBean();
        }

        String name = mNameField.getText().toString();
        mPlaceBean.setName(name);

        String description = mDescriptionField.getText().toString();
        mPlaceBean.setDescription(description);

        LatLng latLng = mMap.getCameraPosition().target;
        if (latLng != null) {
            mPlaceBean.setLatitude(latLng.latitude);
            mPlaceBean.setLongitude(latLng.longitude);
        }

        App app = (App) getApplication();
        Endpoint endpoint = app.getEndpoint();

        mProgressBar.setVisibility(View.VISIBLE);
        mSubscription = AndroidObservable.bindActivity(this, endpoint.savePlace(mPlaceBean)).subscribe(new SaveObserver());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    protected void onSaveInstanceState(@Nonnull Bundle outState) {
        mCenterLatLng = mMap.getCameraPosition().target;
        outState.putParcelable(LAT_LNG, mCenterLatLng);
        super.onSaveInstanceState(outState);
    }

    private class SaveObserver implements Observer<PlaceBean> {

        @Override
        public void onNext(PlaceBean bean) {
            Intent data = new Intent();
            data.putExtra(Constant.PLACE_BEAN, new SerializedPlace(bean));
            EditActivity.this.setResult(Activity.RESULT_OK, data);
        }

        @Override
        public void onCompleted() {
            mProgressBar.setVisibility(View.INVISIBLE);
            EditActivity.this.finish();
        }

        @Override
        public void onError(Throwable e) {
            mSubscription.unsubscribe();
            mProgressBar.setVisibility(View.INVISIBLE);

            Log.e(EditActivity.TAG, e.toString());

            if (e instanceof GoogleJsonResponseException) {
                GoogleJsonResponseException je = (GoogleJsonResponseException) e;
                String message = je.getDetails().getMessage();
                Toast.makeText(EditActivity.this, message, Toast.LENGTH_SHORT).show();
            } else if (e instanceof IOException) {
                Toast.makeText(EditActivity.this, "Network error", Toast.LENGTH_SHORT).show();
            }
        }

    }

}
