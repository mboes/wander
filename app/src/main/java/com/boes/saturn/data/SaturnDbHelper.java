package com.boes.saturn.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.boes.saturn.data.SaturnContract.PersonEntry;
import com.boes.saturn.data.SaturnContract.PlaceEntry;
import com.boes.saturn.data.SaturnContract.SubEntry;

public class SaturnDbHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "saturn.db";

    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_PERSON_TABLE =
            "CREATE TABLE " + PersonEntry.TABLE_NAME + " (" +
            PersonEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            PersonEntry.REMOTE_KEY + " TEXT UNIQUE ON CONFLICT REPLACE, " +
            PersonEntry.NAME + " TEXT NOT NULL" +
            ")";

    private static final String CREATE_PLACE_TABLE =
            "CREATE TABLE " + PlaceEntry.TABLE_NAME + " (" +
            PlaceEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            PlaceEntry.REMOTE_KEY + " TEXT UNIQUE ON CONFLICT REPLACE, " +
            PlaceEntry.NAME + " TEXT NOT NULL, " +
            PlaceEntry.OWNER_ID + " INTEGER NOT NULL, " +
            PlaceEntry.DESCRIPTION + " TEXT, " +
            PlaceEntry.LATITUDE + " REAL NOT NULL, " +
            PlaceEntry.LONGITUDE + " REAL NOT NULL, " +
            PlaceEntry.SUB_COUNT + " INTEGER NOT NULL, " +
            "FOREIGN KEY (" + PlaceEntry.OWNER_ID + ") REFERENCES " + PersonEntry.TABLE_NAME + " (" + PersonEntry._ID + ")" +
            ")";

    private static final String CREATE_SUB_TABLE =
            "CREATE TABLE " + SubEntry.TABLE_NAME + " (" +
            SubEntry._ID + " INTEGER PRIMARY KEY, " +
            SubEntry.SUB_ID + " INTEGER NOT NULL, " +
            SubEntry.PARENT_ID + " INTEGER NOT NULL, " +
            SubEntry.POSITION + " INTEGER NOT NULL, " +
            "FOREIGN KEY (" + SubEntry.SUB_ID + ") REFERENCES " + PlaceEntry.TABLE_NAME + " (" + PlaceEntry._ID + "), " +
            "FOREIGN KEY (" + SubEntry.PARENT_ID + ") REFERENCES " + PlaceEntry.TABLE_NAME + " (" + PlaceEntry._ID + "), " +
            "UNIQUE (" + SubEntry.SUB_ID + ", " + SubEntry.PARENT_ID + ") ON CONFLICT REPLACE" +
            ")";

    private static final String DROP_PERSON_TABLE =
            "DROP TABLE IF EXISTS " + PersonEntry.TABLE_NAME;

    private static final String DROP_PLACE_TABLE =
            "DROP TABLE IF EXISTS " + PlaceEntry.TABLE_NAME;

    private static final String DROP_SUB_TABLE =
            "DROP TABLE IF EXISTS " + SubEntry.TABLE_NAME;

    public SaturnDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PERSON_TABLE);
        db.execSQL(CREATE_PLACE_TABLE);
        db.execSQL(CREATE_SUB_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_PERSON_TABLE);
        db.execSQL(DROP_PLACE_TABLE);
        db.execSQL(DROP_SUB_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
