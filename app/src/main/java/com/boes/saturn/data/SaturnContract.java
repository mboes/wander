package com.boes.saturn.data;

import android.provider.BaseColumns;

public class SaturnContract {

    public static final class PersonEntry implements BaseColumns {

        public static final String TABLE_NAME = "person";

        public static final String REMOTE_KEY = "remote_key";

        public static final String NAME = "name";

    }

    public static final class PlaceEntry implements BaseColumns {

        public static final String TABLE_NAME = "place";

        public static final String REMOTE_KEY = "remote_key";

        public static final String NAME = "name";

        public static final String OWNER_ID = "owner_id";

        public static final String DESCRIPTION = "description";

        public static final String LATITUDE = "latitude";

        public static final String LONGITUDE = "longitude";

        public static final String SUB_COUNT = "sub_count";

    }

    public static final class SubEntry implements BaseColumns {

        public static final String TABLE_NAME = "sub";

        public static final String SUB_ID = "sub_id";

        public static final String PARENT_ID = "parent_id";

        public static final String POSITION = "position";

    }

}
