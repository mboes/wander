package com.boes.saturn.data;

import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;

import java.io.IOException;
import java.io.Serializable;

public class SerializedPlace implements Serializable {

    private static final long serialVersionUID = 1L;

    private String json;

    public SerializedPlace(PlaceBean bean) {
        json = bean.toString();
    }

    public PlaceBean getBean() {
        AndroidJsonFactory factory = new AndroidJsonFactory();
        PlaceBean bean;

        try {
            bean = factory.fromString(json, PlaceBean.class);
        } catch (IOException e) {
            bean = new PlaceBean();
        }

        return bean;
    }

}
