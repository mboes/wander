package com.boes.saturn.endpoint;

import android.content.SharedPreferences;
import android.util.Log;

import com.boes.saturn.backend.saturn.Saturn;
import com.boes.saturn.backend.saturn.model.PersonBean;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.interfaces.Endpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;
import rx.schedulers.Schedulers;
import rx.util.async.Async;

public class CloudEndpoint implements Endpoint {

    private static final String TAG = CloudEndpoint.class.getSimpleName();

    private Saturn api;
    private SharedPreferences prefs;

    public CloudEndpoint(Saturn api, SharedPreferences prefs) {
        this.api = api;
        this.prefs = prefs;
    }

    @Override
    public Observable<PersonBean> getPerson(final String webSafeKey) {
        Callable<PersonBean> callable = new Callable<PersonBean>() {
            @Override
            public PersonBean call() throws Exception {
                Log.d(TAG, "Calling api getPerson...");
                PersonBean bean = api.getPerson(webSafeKey).execute();
                Log.d(TAG, "Returning found person...");
                return bean;
            }
        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<PersonBean> savePerson(final PersonBean bean) {
        Callable<PersonBean> callable = new Callable<PersonBean>() {

            @Override
            public PersonBean call() throws Exception {
                Log.d(TAG, "Calling api savePerson...");
                PersonBean saved =  api.savePerson(bean).execute();
                Log.d(TAG, "Returning saved person...");
                return saved;
            }

        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<List<PlaceBean>> findPlaces(final int limit) {
        Callable<List<PlaceBean>> callable = new Callable<List<PlaceBean>>() {

            @Override
            public List<PlaceBean> call() throws Exception {
                getUserWebSafeKey();

                Log.d(TAG, "Calling api findPlaces...");
                List<PlaceBean> beans = api.findPlaces(limit).execute().getItems();
                Log.d(TAG, "Returning found places...");
                return nullToEmpty(beans);
            }

        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<List<PlaceBean>> getSubPlaces(final String webSafeKey) {
        Callable<List<PlaceBean>> callable = new Callable<List<PlaceBean>>() {
            @Override
            public List<PlaceBean> call() throws Exception {
                Log.d(TAG, "Calling api getSubPlaces...");
                List<PlaceBean> beans =  api.getSubPlaces(webSafeKey).execute().getItems();
                Log.d(TAG, "Returning found sub places...");
                return nullToEmpty(beans);
            }
        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<List<PlaceBean>> getPlacesByPerson(final String webSafeKey) {
        Callable<List<PlaceBean>> callable = new Callable<List<PlaceBean>>() {

            @Override
            public List<PlaceBean> call() throws Exception {
                String personKey = webSafeKey;
                if (personKey == null) {
                    personKey = getUserWebSafeKey();
                }

                Log.d(TAG, "Calling api getPlacesByPerson...");
                List<PlaceBean> beans = api.getPlacesByPerson(personKey).execute().getItems();
                Log.d(TAG, "Returning found places by person");
                return nullToEmpty(beans);
            }

        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<PlaceBean> getPlace(final String webSafeKey) {
        Callable<PlaceBean> callable = new Callable<PlaceBean>() {
            @Override
            public PlaceBean call() throws Exception {
                Log.d(TAG, "Calling api getPlace...");
                PlaceBean bean = api.getPlace(webSafeKey).execute();
                Log.d(TAG, "Returning found place...");
                return bean;
            }
        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<PlaceBean> savePlace(final PlaceBean bean) {
        Callable<PlaceBean> callable = new Callable<PlaceBean>() {
            @Override
            public PlaceBean call() throws Exception {
                Log.d(TAG, "Calling api savePlace...");
                PlaceBean saved = api.savePlace(bean).execute();
                Log.d(TAG, "Returning saved place...");
                return saved;
            }
        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<Boolean> deletePlace(final String webSafeKey) {
        Callable<Boolean> callable = new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d(TAG, "Calling api deletePlace...");
                api.deletePlace(webSafeKey).execute();
                Log.d(TAG, "Deleted place...");
                return true;
            }
        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    @Override
    public Observable<Boolean> ratePlace(final String webSafeKey, final int rating) {
        Callable<Boolean> callable = new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                Log.d(TAG, "Calling api ratePlace...");
                api.ratePlace(webSafeKey, rating).execute();
                Log.d(TAG, "Rated place...");
                return true;
            }
        };

        return Async.fromCallable(callable, Schedulers.io());
    }

    private String getUserWebSafeKey() throws Exception {
        String webSafeKey = prefs.getString("webSafeKey", null);

        if (webSafeKey == null) {
            PersonBean bean = new PersonBean();
            Log.d(TAG, "Calling api savePerson...");
            webSafeKey = api.savePerson(bean).execute().getWebSafeKey();
            prefs.edit().putString("webSafeKey", webSafeKey).apply();
        }

        return webSafeKey;
    }

    private List<PlaceBean> nullToEmpty(List<PlaceBean> beans) {
        if (beans != null) {
            return beans;
        } else {
            return new ArrayList<PlaceBean>();
        }
    }

}
