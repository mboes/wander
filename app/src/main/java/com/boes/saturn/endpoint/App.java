package com.boes.saturn.endpoint;

import android.accounts.Account;
import android.app.Application;
import android.content.SharedPreferences;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.Saturn;
import com.boes.saturn.backend.saturn.model.PersonBean;
import com.boes.saturn.interfaces.Endpoint;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.android.json.AndroidJsonFactory;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;

public class App extends Application {

    private SharedPreferences prefs;
    private GoogleAccountCredential credential;
    private Endpoint endpoint;

    @Override
    public void onCreate() {
        super.onCreate();

        prefs = getSharedPreferences("prefs", MODE_PRIVATE);

        String audience = "server:client_id:" + getString(R.string.web_key);
        credential = GoogleAccountCredential.usingAudience(this, audience);
        credential.setSelectedAccountName(getUserAccount());

        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = new AndroidJsonFactory();
        Saturn.Builder builder = new Saturn.Builder(transport, jsonFactory, credential);
        builder.setApplicationName("com.boes.saturn");
        Saturn api = builder.build();

        endpoint = new CloudEndpoint(api, prefs);
    }

    public String getUserAccount() {
        String account = prefs.getString("account", null);
        if (account == null) {
            account = getDefaultUserAccount();
            prefs.edit().putString("account", account).apply();
        }
        return account;
    }

    private String getDefaultUserAccount() {
        Account[] accounts = credential.getAllAccounts();
        return accounts[0].name;
    }

    public void setUserAccount(String account) {
        prefs.edit().putString("account", account)
                    .putString("webSafeKey", null)
                    .apply();
        credential.setSelectedAccountName(account);
    }

    public Endpoint getEndpoint() {
        return endpoint;
    }

    public boolean isUserOwner(PersonBean owner) {
        String webSafeKey = prefs.getString("webSafeKey", null);
        return owner.getWebSafeKey().equals(webSafeKey);
    }

}
