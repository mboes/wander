package com.boes.saturn.views;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;
import com.boes.saturn.controllers.PersonActivity;
import com.boes.saturn.controllers.ViewActivity;
import com.boes.saturn.data.SerializedPlace;
import com.boes.saturn.interfaces.Constant;
import com.google.maps.android.ui.IconGenerator;

public class ViewUtils {

    private static final String TAG = ViewUtils.class.getSimpleName();

    public static View getPlaceView(LayoutInflater inflater, int layout, ViewGroup container) {
        return inflater.inflate(layout, container, false);
    }

    public static void initPlaceItem(final Context context, View v, final PlaceBean bean) {
        setItemColor(bean, v);

        TextView tvName = (TextView) v.findViewById(R.id.tvName);
        tvName.setText(bean.getName());

        TextView btnSubCount = (TextView) v.findViewById(R.id.tvSubCount);
        btnSubCount.setText(bean.getSubCount().toString());
        btnSubCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ViewActivity.class);
                i.putExtra(Constant.PLACE_BEAN, new SerializedPlace(bean));
                context.startActivity(i);
            }
        });

        TextView tvDescription = (TextView) v.findViewById(R.id.tvDescription);
        tvDescription.setText(bean.getDescription());

        Button btnOwner = (Button) v.findViewById(R.id.btnOwner);
        btnOwner.setText(bean.getOwner().getName());
        btnOwner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, PersonActivity.class);
                i.putExtra(Constant.PERSON_NAME, bean.getOwner().getName());
                i.putExtra(Constant.WEB_SAFE_KEY, bean.getOwner().getWebSafeKey());
                context.startActivity(i);
            }
        });

        Button btnWeb = (Button) v.findViewById(R.id.btnWeb);
        btnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_WEB_SEARCH);
                i.putExtra(SearchManager.QUERY, bean.getName());
                context.startActivity(i);
            }
        });

        Button btnDistance = (Button) v.findViewById(R.id.btnDistance);
        btnDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://maps.google.com/maps?daddr=" + bean.getLatitude() + "," + bean.getLongitude();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                context.startActivity(i);
            }
        });
    }

    private static void setItemColor(PlaceBean bean, View v) {
        if (bean.getSubCount() > 0) {
            v.setBackgroundResource(R.drawable.item_guide_bg);
        } else {
            v.setBackgroundResource(R.drawable.item_place_bg);
        }
    }

    public static void setMarkerStyle(IconGenerator factory, PlaceBean bean) {
        if (bean != null && bean.getSubCount() > 0) {
            factory.setStyle(IconGenerator.STYLE_BLUE);
        } else {
            factory.setStyle(IconGenerator.STYLE_PURPLE);
        }
    }

}
