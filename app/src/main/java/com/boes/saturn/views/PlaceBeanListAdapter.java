package com.boes.saturn.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.boes.saturn.R;
import com.boes.saturn.backend.saturn.model.PlaceBean;

public class PlaceBeanListAdapter extends ArrayAdapter<PlaceBean> {

    public PlaceBeanListAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = ViewUtils.getPlaceView(inflater, R.layout.item_place, parent);
        }

        ViewUtils.initPlaceItem(getContext(), convertView, getItem(position));

        return convertView;
    }

}
