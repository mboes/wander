package com.boes.saturn.views;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

// See YouTube video When Android Meets Maps for more detail and examples on sensor code
// https://www.youtube.com/watch?v=_oZiK_NJuG8

public class BearingImageView extends ImageView implements SensorEventListener {

    private static final String TAG = BearingImageView.class.getSimpleName();

    private SensorManager mSensorManager;
    private Sensor mRotationSensor;
    private float[] mRotationMatrix = new float[16];
    private float[] mRotationValues = new float[3];

    public BearingImageView(Context context) {
        super(context);
        init();
    }

    public BearingImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BearingImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        Context context = getContext();
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
    }

    public void registerSensor() {
        mSensorManager.registerListener(this, mRotationSensor, 8000);  // 30 Hz
        Log.d(TAG, "Registered listener");
    }

    public void unregisterSensor() {
        mSensorManager.unregisterListener(this);
        Log.d(TAG, "Unregistered listener");
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        SensorManager.getRotationMatrixFromVector(mRotationMatrix, event.values);
        SensorManager.getOrientation(mRotationMatrix, mRotationValues);

        float azimuth = (float) Math.toDegrees(mRotationValues[0]);
        Log.d(TAG, "Azimuth = " + azimuth);

        setRotation(azimuth);  // setRotationX ??
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
