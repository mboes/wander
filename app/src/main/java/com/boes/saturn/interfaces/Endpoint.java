package com.boes.saturn.interfaces;

import com.boes.saturn.backend.saturn.model.PersonBean;
import com.boes.saturn.backend.saturn.model.PlaceBean;

import java.util.List;

import rx.Observable;

public interface Endpoint {
    public Observable<PersonBean> getPerson(String webSafeKey);
    public Observable<PersonBean> savePerson(PersonBean bean);

    public Observable<List<PlaceBean>> findPlaces(int limit);
    public Observable<List<PlaceBean>> getSubPlaces(String webSafeKey);
    public Observable<List<PlaceBean>> getPlacesByPerson(String webSafeKey);

    public Observable<PlaceBean> getPlace(String webSafeKey);
    public Observable<PlaceBean> savePlace(PlaceBean bean);
    public Observable<Boolean> deletePlace(String webSafeKey);

    public Observable<Boolean> ratePlace(String webSafeKey, int rating);
}
