package com.boes.saturn.interfaces;

import android.location.Location;

public interface OnLocationRequestListener {
    public Location getLocation();
}
