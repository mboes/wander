package com.boes.saturn.interfaces;

public class Constant {
    public static final String PLACE_BEAN = "place";
    public static final String WEB_SAFE_KEY = "web_safe_key";
    public static final String PLACE_NAME = "place_name";
    public static final String PERSON_NAME = "person_name";
}
