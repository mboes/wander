package com.boes.saturn.interfaces;

import com.boes.saturn.backend.saturn.model.PlaceBean;

public interface PlaceListener {
    public void onPlacePicked(PlaceBean bean);
    public void onRequest();
    public void onCompleted();
    public void onError(String tag, Throwable e, Runnable callback);
    public void setStatusMessage(String message);
}
