# Wander #

Wander is a user-generated travel guide app.  Users may create points of interest that they might have some knowledge of, compose a tweet-length description of those GPS points, and organize them recursively into groups by including multiple points inside another point of interest.

### Screenshots ###

![List of places](https://bytebucket.org/mboes/wander/raw/db6a2e460c12812bf7ec3cec34aacd98e4d70fd5/screenshots/list.png "List of places")
![New place](https://bytebucket.org/mboes/wander/raw/db6a2e460c12812bf7ec3cec34aacd98e4d70fd5/screenshots/new.png "New place")

![Guide] (https://bytebucket.org/mboes/wander/raw/db6a2e460c12812bf7ec3cec34aacd98e4d70fd5/screenshots/guide.png "Guide")
![Sub place](https://bytebucket.org/mboes/wander/raw/db6a2e460c12812bf7ec3cec34aacd98e4d70fd5/screenshots/subplace.png "Sub place")

### Tech ###

* Runs on Google Cloud Endpoints, App Engine, and Datastore
* Uses Objectify for key-value style ORM
* Uses RxJava Observables for app REST interface
* Uses Google Account on phone for transparent user authentication

### Configure and Run ###

* Clone repo and use Android Studio
* Create new project on [Google Developers Console](https://console.developers.google.com)
* Turn on Android Maps API in console
* Update application tag in backend/src/main/webapp/WEB-INF/appengine-web.xml with Google Project ID
* Create ApiKey class in com.boes.saturn.backend.constant package with required Google keys for project (used in SaturnEndpoint class)
* Create keys.xml file in res/values of Android app with web key (used in App class) and maps key (used in manifest)
* See [docs](https://cloud.google.com/appengine/docs/java/endpoints/getstarted/clients/android/add_auth_ids) for help with generating and setting up keys
* See [tutorial](https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints) for how to locally test and deploy a new backend
