package com.boes.saturn.backend.entity;

import com.boes.saturn.backend.bean.PersonBean;
import com.boes.saturn.backend.bean.PlaceBean;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Ref;
import com.googlecode.objectify.Work;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.googlecode.objectify.annotation.Parent;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.boes.saturn.backend.OfyService.ofy;

@Entity
public class PlaceEntity {

    @Id
    private Long id;

    @Parent @Load
    private Ref<PersonEntity> owner;

    private String name;
    private String description;
    private double latitude;
    private double longitude;

    @Index
    private Date timestamp;

    @Index
    private LinkedHashSet<Key<PlaceEntity>> subKeys = new LinkedHashSet<Key<PlaceEntity>>();

    @Index
    private int ratingSum;
    private int ratingCount;

    @Ignore
    private User requestedBy;

    @Ignore
    private PlaceEntity parent;

    private PlaceEntity() {}

    public PlaceEntity(User user) {
        PersonEntity person = PersonEntity.getOrCreate(user);
        owner = Ref.create(person);
    }

    public static PlaceEntity get(Key<PlaceEntity> key) throws NotFoundException {
        PlaceEntity entity = ofy().load().key(key).now();

        if (entity == null) {
            throw new NotFoundException("Place not found");
        }

        return entity;
    }

    public static PlaceEntity get(User user, Key<PlaceEntity> key) throws UnauthorizedException, NotFoundException {
        PlaceEntity.verifyUserIsOwner(user, key);
        return PlaceEntity.get(key);
    }

    private static void verifyUserIsOwner(User user, Key<PlaceEntity> placeKey) throws UnauthorizedException {
        Key<PersonEntity> userKey = PersonEntity.generateKey(user);
        Key<PersonEntity> ownerKey = placeKey.getParent();

        if (!userKey.equivalent(ownerKey)) {
            throw new UnauthorizedException("Access denied");
        }
    }

    public static void delete(User user, Key<PlaceEntity> key) throws UnauthorizedException {
        PlaceEntity.verifyUserIsOwner(user, key);
        ofy().delete().key(key).now();
    }

    public static void ratePlace(final User user, final Key<PlaceEntity> key, final int rating) {
        ofy().transact(new Work<Void>() {

            @Override
            public Void run() {
                try {
                    PlaceEntity place = PlaceEntity.get(key);
                    PersonEntity person = PersonEntity.getOrCreate(user);

                    int oldRating = person.addRating(key, rating);
                    if (oldRating != 0) {
                        place.removeRating(oldRating);
                    }

                    place.addRating(rating);
                    ofy().save().entities(place, person);
                } catch (NotFoundException e) {
                    System.err.println("Place to rate not found");
                    throw new RuntimeException(e);
                }
                return null;
            }

        });
    }

    public void addRating(int rating) {
        ratingSum += rating;
        ratingCount++;
    }

    public void removeRating(int rating) {
        ratingSum -= rating;
        ratingCount--;
    }

    public static Map<Key<PlaceEntity>, PlaceEntity> getPlaces(List<Key<PlaceEntity>> keys) {
        return ofy().load().keys(keys);
    }

    public static List<PlaceEntity> findPlaces(int limit) {
        limit = Math.min(limit, 100);
        return ofy().load().type(PlaceEntity.class).limit(limit).list();
    }

    public void update(PlaceBean bean) throws BadRequestException {
        setName(bean.name);
        setDescription(bean.description);
        setLatLng(bean.latitude, bean.longitude);
        setTimestamp(bean.timestamp);
        addToSubKeys(bean.subKeysToAdd);
        removeFromSubKeys(bean.subKeysToRemove);
    }

    public void save() throws BadRequestException {
        checkName();
        checkLatLng();
        ofy().save().entity(this).now();
    }

    private void setName(String name) {
        if (name != null) {
            name = name.trim();
            if (!name.isEmpty()) {
                this.name = name;
            }
        }
    }

    private void setDescription(String description) {
        if (description != null) {
            description = description.trim();
            int length = description.length();

            if (length == 0) {
                description = null;
            } else if (length > 140) {
                description = description.substring(0, 140);
            }

            this.description = description;
        }
    }

    private void setLatLng(double latitude, double longitude) {
        if (latitude != 0.0 && longitude != 0.0) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    private void setTimestamp(Date timestamp) {
        if (timestamp != null) {
            this.timestamp = timestamp;
        } else {
            this.timestamp = new Date();
        }
    }

    private void addToSubKeys(List<String> subKeysToAdd) throws BadRequestException {
        if (subKeysToAdd != null) {
            Key<PlaceEntity> parent = null;
            if (id != null) {
                parent = getKey();
            }

            LinkedHashSet<Key<PlaceEntity>> keys = new LinkedHashSet<Key<PlaceEntity>>();
            for (String webSafeKey : subKeysToAdd) {
                Key<PlaceEntity> key = Key.create(webSafeKey);
                if (parent != null && parent.equivalent(key)) {
                    throw new BadRequestException("Place can't include itself");
                }

                if (subKeys.contains(key)) {
                    throw new BadRequestException("Place already included");
                }

                if (!keys.add(key)) {
                    throw new BadRequestException("Duplicate place");
                }
            }
            subKeys.addAll(keys);
        }
    }

    private void removeFromSubKeys(List<String> subKeysToRemove) {
        if (subKeysToRemove != null) {
            for (String webSafeKey : subKeysToRemove) {
                Key<PlaceEntity> key = Key.create(webSafeKey);
                subKeys.remove(key);
            }
        }
    }

    private void checkName() throws BadRequestException {
        if (name == null) {
            throw new BadRequestException("Name required");
        }
    }

    private void checkLatLng() throws BadRequestException {
        if (latitude == 0.0 || longitude == 0.0) {
            throw new BadRequestException("Latitude and longitude required");
        }

        if (latitude < -90.0 || latitude > 90.0) {
            throw new BadRequestException("Invalid latitude");
        }

        if (longitude < -180.0 || longitude >= 180.0) {
            throw new BadRequestException("Invalid longitude");
        }
    }

    public String getWebSafeKey() {
        Key<PlaceEntity> key = getKey();
        return key.getString();
    }

    private Key<PlaceEntity> getKey() {
        return Key.create(owner.key(), PlaceEntity.class, id);
    }

    public String getName() {
        return name;
    }

    public PersonBean getOwner() {
        PersonEntity entity = owner.get();
        return PersonBean.fromEntity(entity);
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getSubCount() {
        return subKeys.size();
    }

    public Collection<PlaceEntity> getSubPlaces() {
        Map<Key<PlaceEntity>, PlaceEntity> keysToEntities = ofy().load().keys(subKeys);
        removeSubKeysOfMissingEntities(keysToEntities);
        return keysToEntities.values();
    }

    private void removeSubKeysOfMissingEntities(Map<Key<PlaceEntity>, PlaceEntity> keysToEntities) {
        int nSubKeys = subKeys.size();
        int nEntities = keysToEntities.size();

        if (nSubKeys != nEntities) {
            subKeys.retainAll(keysToEntities.keySet());
            ofy().save().entity(this);
        }
    }

    public String getDescription() {
        return description;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

//    public Collection<PlaceEntity> getIncludedIn(int limit) {
//        Key<PlaceEntity> key = getKey();
//
//        Collection<PlaceEntity> results =
//                ofy().load().type(PlaceEntity.class)
//                     .filter("subKeys", key)
//                     .order("-ratingSum")
//                     .limit(limit + 1).list();
//
//        results.remove(parent);
//        List<PlaceEntity> includedIn = new ArrayList<PlaceEntity>();
//
//        int i = 0;
//        for (PlaceEntity result : results) {
//            if (i < limit) {
//                includedIn.add(result);
//            } else {
//                break;
//            }
//            i++;
//        }
//        return includedIn;
//    }

    public void setParent(PlaceEntity parent) {
        this.parent = parent;
    }

    public int getAverageRating() {
        if (ratingCount == 0) {
            return 0;
        } else {
            return Math.round(ratingSum / ratingCount);
        }
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public int getUserRating() {
        if (requestedBy == null) {
            return 0;
        } else {
            PersonEntity person = PersonEntity.getOrCreate(requestedBy);
            Key<PlaceEntity> key = getKey();
            return person.getRating(key);
        }
    }

    public void setRequestUser(User user) {
        requestedBy = user;
    }

}
