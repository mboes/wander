package com.boes.saturn.backend.bean;

import com.boes.saturn.backend.entity.PlaceEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlaceBean {

    public String webSafeKey;
    public String name;
    public PersonBean owner;

    public Date timestamp;

    public List<String> subKeysToAdd;
    public List<String> subKeysToRemove;
    public int subCount;

    public String description;

    public double latitude;
    public double longitude;

    public int averageRating;
    public int ratingCount;
    public int userRating;

    public List<PlaceBean> includedIn;

    public static PlaceBean fromEntity(PlaceEntity entity) {
        PlaceBean bean = new PlaceBean();
        bean.webSafeKey = entity.getWebSafeKey();
        bean.name = entity.getName();
        bean.owner = entity.getOwner();
        bean.timestamp = entity.getTimestamp();
        bean.subCount = entity.getSubCount();
        bean.description = entity.getDescription();
        bean.latitude = entity.getLatitude();
        bean.longitude = entity.getLongitude();
        bean.averageRating = entity.getAverageRating();
        bean.ratingCount = entity.getRatingCount();
        bean.userRating = entity.getUserRating();
        bean.includedIn = new ArrayList<PlaceBean>();  // = PlaceBean.fromEntities(entity.getIncludedIn(2));
        return bean;
    }

    public static List<PlaceBean> fromEntities(Collection<PlaceEntity> entities) {
        List<PlaceBean> beans = new ArrayList<PlaceBean>();
        for (PlaceEntity entity : entities) {
            beans.add(PlaceBean.fromEntity(entity));
        }
        return beans;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PlaceBean)) {
            return false;
        }

        PlaceBean that = (PlaceBean) obj;
        return this.webSafeKey.equals(that.webSafeKey) &&
               this.name.equals(that.name) &&
               this.owner.equals(that.owner) &&
               safeEquals(this.timestamp, that.timestamp) &&
               this.subCount == that.subCount &&
               safeEquals(this.description, that.description) &&
               this.latitude == that.latitude &&
               this.longitude == that.longitude &&
               this.averageRating == that.averageRating &&
               this.ratingCount == that.ratingCount &&
               this.userRating == that.userRating &&
               this.includedIn.equals(that.includedIn);
    }

    private boolean safeEquals(String s1, String s2) {
        return s1 == null ? s2 == null : s1.equals(s2);
    }

    private boolean safeEquals(Date d1, Date d2) {
        return d1 == null ? d2 == null : d1.equals(d2);
    }

    @Override
    public String toString() {
        Map<String, Object> state = new HashMap<String, Object>();
        state.put("webSafeKey", webSafeKey);
        state.put("name", name);
        state.put("owner", owner);
        state.put("timestamp", timestamp);
        state.put("subCount", subCount);
        state.put("description", description);
        state.put("latitude", latitude);
        state.put("longitude", longitude);
        state.put("averageRating", averageRating);
        state.put("ratingCount", ratingCount);
        state.put("userRating", userRating);
        state.put("includedIn", includedIn);
        return state.toString();
    }

}
