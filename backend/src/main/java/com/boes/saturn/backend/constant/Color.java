package com.boes.saturn.backend.constant;

public enum Color {
    BLUE,
    GREEN,
    YELLOW,
    PINK,
    PURPLE,
    RED
}
