package com.boes.saturn.backend.entity;

import com.boes.saturn.backend.bean.PersonBean;
import com.boes.saturn.backend.constant.Color;
import com.google.api.server.spi.response.NotFoundException;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Stringify;
import com.googlecode.objectify.stringifier.KeyStringifier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.boes.saturn.backend.OfyService.ofy;

@Entity
public class PersonEntity {

    @Id
    private String email;

    private String name;

    private Color favoriteColor;

    @Stringify(KeyStringifier.class)
    private Map<Key<PlaceEntity>, Integer> ratings = new HashMap<Key<PlaceEntity>, Integer>();

    @Ignore
    private static final Random RANDOM = new Random();

    private PersonEntity() {}

    public PersonEntity(User user) {
        String email = user.getEmail();
        String defaultName = email.substring(0, email.indexOf("@"));
        this.email = email.toLowerCase();
        this.name = defaultName;

        Color[] colors = Color.values();
        int pick = RANDOM.nextInt(colors.length);
        this.favoriteColor = colors[pick];
    }

    public static PersonEntity get(Key<PersonEntity> key) throws NotFoundException {
        PersonEntity entity = ofy().load().key(key).now();

        if (entity == null) {
            throw new NotFoundException("Person not found");
        }

        return entity;
    }

    public static PersonEntity get(User user) throws NotFoundException {
        Key<PersonEntity> key = PersonEntity.generateKey(user);
        return PersonEntity.get(key);
    }

    public static PersonEntity getOrCreate(User user) {
        PersonEntity entity;

        try {
            entity = PersonEntity.get(user);
        } catch (NotFoundException e) {
            entity = new PersonEntity(user);
            entity.save();
        }

        return entity;
    }

    public static Key<PersonEntity> generateKey(User user) {
        String email = user.getEmail();
        return Key.create(PersonEntity.class, email.toLowerCase());
    }

    public static List<PlaceEntity> getPlacesByPerson(Key<PersonEntity> key) {
        return ofy().load().type(PlaceEntity.class).ancestor(key).order("-timestamp").list();
    }

    public void update(PersonBean bean) {
        setName(bean.name);
        setFavoriteColor(bean.favoriteColor);
    }

    private void setName(String name) {
        if (name != null) {
            name = name.trim();
            if (!name.isEmpty()) {
                this.name = name;
            }
        }
    }

    private void setFavoriteColor(Color color) {
        if (color != null) {
            this.favoriteColor = color;
        }
    }

    public int addRating(Key<PlaceEntity> key, int rating) {
        Integer old = ratings.put(key, rating);
        return old == null ? 0 : old;
    }

    public int getRating(Key<PlaceEntity> key) {
        Integer rating = ratings.get(key);
        return rating == null ? 0 : rating;
    }

    public void save() {
        ofy().save().entity(this).now();
    }

    public String getName() {
        return name;
    }

    public String getWebSafeKey() {
        Key<PersonEntity> key = Key.create(PersonEntity.class, email);
        return key.getString();
    }

    public Color getFavoriteColor() {
        return favoriteColor;
    }

}
