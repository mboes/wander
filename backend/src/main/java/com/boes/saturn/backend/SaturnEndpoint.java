package com.boes.saturn.backend;

import com.boes.saturn.backend.bean.PersonBean;
import com.boes.saturn.backend.bean.PlaceBean;
import com.boes.saturn.backend.constant.ApiKey;
import com.boes.saturn.backend.entity.PersonEntity;
import com.boes.saturn.backend.entity.PlaceEntity;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.search.Document;
import com.google.appengine.api.search.Field;
import com.google.appengine.api.search.GeoPoint;
import com.google.appengine.api.search.Index;
import com.google.appengine.api.search.IndexSpec;
import com.google.appengine.api.search.PutException;
import com.google.appengine.api.search.Query;
import com.google.appengine.api.search.QueryOptions;
import com.google.appengine.api.search.Results;
import com.google.appengine.api.search.ScoredDocument;
import com.google.appengine.api.search.SearchServiceFactory;
import com.google.appengine.api.search.StatusCode;
import com.google.appengine.api.users.User;
import com.googlecode.objectify.Key;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Named;

@Api(name = "saturn",
     description = "Api for places in Galaxy",
     version = "v1",
     clientIds = { ApiKey.WEB, ApiKey.ANDROID, ApiKey.EXPLORER },
     audiences = { ApiKey.AUDIENCE },
     namespace = @ApiNamespace(ownerDomain = "backend.saturn.boes.com", ownerName = "backend.saturn.boes.com", packagePath=""))
public class SaturnEndpoint {

    // Flag set to false during testing, where Search APIs are not available on local dev machine
    private static final boolean ENABLE_SEARCH = false;

    @ApiMethod(name = "getPlace", path = "place/{webSafeKey}", httpMethod = ApiMethod.HttpMethod.GET)
    public PlaceBean getPlace(User user, @Named("webSafeKey") String webSafeKey) throws BadRequestException, NotFoundException {
        if (webSafeKey == null) {
            throw new BadRequestException("webSafeKey required");
        }

        Key<PlaceEntity> key = Key.create(webSafeKey);
        PlaceEntity entity = PlaceEntity.get(key);
        entity.setRequestUser(user);

        return PlaceBean.fromEntity(entity);
    }

    @ApiMethod(name = "savePlace", path = "place", httpMethod = ApiMethod.HttpMethod.POST)
    public PlaceBean savePlace(User user, PlaceBean bean) throws UnauthorizedException, BadRequestException, NotFoundException {
        if (user == null) {
            throw new UnauthorizedException("Authentication required");
        }

        if (bean == null) {
            throw new BadRequestException("PlaceBean is null");
        }

        String webSafeKey = bean.webSafeKey;
        PlaceEntity entity;

        if (webSafeKey != null) {
            Key<PlaceEntity> key = Key.create(webSafeKey);
            entity = PlaceEntity.get(user, key);
        } else {
            entity = new PlaceEntity(user);
        }

        entity.update(bean);
        entity.save();

        if (ENABLE_SEARCH) {
            // Update the search index
            Document document = Document.newBuilder()
                    .setId(entity.getWebSafeKey())  // Setting the document identifier is optional.  If omitted, the search service will create an identifier.
                    .addField(Field.newBuilder().setName("name").setText(entity.getName()))
                    .addField(Field.newBuilder().setName("description").setText(entity.getDescription()))
                    .addField(Field.newBuilder().setName("location").setGeoPoint(new GeoPoint(entity.getLatitude(), entity.getLongitude())))
                    .build();

            try {
                getPlaceIndex().put(document);
            } catch (PutException e) {
                if (StatusCode.TRANSIENT_ERROR.equals(e.getOperationResult().getCode())) {
                    // Retry putting the document
                }
            }
        }

        return PlaceBean.fromEntity(entity);
    }

    @ApiMethod(name = "getPerson", path = "person/{webSafeKey}", httpMethod = ApiMethod.HttpMethod.GET)
    public PersonBean getPerson(@Named("webSafeKey") String webSafeKey) throws BadRequestException, NotFoundException {
        if (webSafeKey == null) {
            throw new BadRequestException("webSafeKey required");
        }

        Key<PersonEntity> key = Key.create(webSafeKey);
        PersonEntity entity = PersonEntity.get(key);
        return PersonBean.fromEntity(entity);
    }

    @ApiMethod(name = "savePerson", path = "person", httpMethod = ApiMethod.HttpMethod.POST)
    public PersonBean savePerson(User user, PersonBean bean) throws UnauthorizedException, BadRequestException {
        if (user == null) {
            throw new UnauthorizedException("Authentication required");
        }

        if (bean == null) {
            throw new BadRequestException("PersonBean is null");
        }

        PersonEntity entity;
        try {
            entity = PersonEntity.get(user);
        } catch (NotFoundException e) {
            entity = new PersonEntity(user);
        }

        entity.update(bean);
        entity.save();
        return PersonBean.fromEntity(entity);
    }

    @ApiMethod(name = "getPlacesByPerson", path = "place/person/{webSafeKey}", httpMethod = ApiMethod.HttpMethod.GET)
    public List<PlaceBean> getPlacesByPerson(@Named("webSafeKey") String webSafeKey) throws NotFoundException {
        Key<PersonEntity> key = Key.create(webSafeKey);
        return PlaceBean.fromEntities(PersonEntity.getPlacesByPerson(key));
    }

    @ApiMethod(name = "deletePlace", path = "place/{webSafeKey}", httpMethod = ApiMethod.HttpMethod.DELETE)
    public void deletePlace(User user, @Named("webSafeKey") String webSafeKey) throws UnauthorizedException, BadRequestException {
        if (user == null) {
            throw new UnauthorizedException("Authentication required");
        }

        if (webSafeKey == null) {
            throw new BadRequestException("webSafeKey required");
        }

        Key<PlaceEntity> key = Key.create(webSafeKey);
        PlaceEntity.delete(user, key);

        if (ENABLE_SEARCH) {
            // Remove it from the search index;
            getPlaceIndex().delete(Collections.singletonList(webSafeKey));
        }
    }

    @ApiMethod(name = "findPlaces", path = "place", httpMethod = ApiMethod.HttpMethod.GET)
    public List<PlaceBean> findPlaces(@Named("limit") int limit) {
        return PlaceBean.fromEntities(PlaceEntity.findPlaces(limit));
    }

    /**
     * Get the nearby places
     * @param lat
     * @param lon
     * @param distance - In meters
     * @param limit - Max 100
     * @return
     */
    @ApiMethod(name = "findNearbyPlaces", path = "nearby_places")
    public List<PlaceBean> findNearbyPlaces(@Named("latitude") double lat, @Named("longitude") double lon,
                                            @Named("distance") int distance, @Named("limit") int limit) {
        limit = Math.min(limit, 100);
        QueryOptions options = QueryOptions.newBuilder().setLimit(limit).build();
        Query q = Query.newBuilder().setOptions(options).build("distance(location, geopoint(" + lat + ", " + lon + ")) < " + distance);
        Results<com.google.appengine.api.search.ScoredDocument> results = getPlaceIndex().search(q);

        List<Key<PlaceEntity>> keys = new ArrayList<Key<PlaceEntity>>();
        for(ScoredDocument sd : results) {
            Key<PlaceEntity> key = Key.create(sd.getId());
            keys.add(key);
        }

        return PlaceBean.fromEntities(PlaceEntity.getPlaces(keys).values());
    }

    @ApiMethod(name = "getSubPlaces", path = "place/{webSafeKey}/sub", httpMethod = ApiMethod.HttpMethod.GET)
    public List<PlaceBean> getSubPlaces(User user, @Named("webSafeKey") String webSafeKey) throws BadRequestException, NotFoundException {
        if (webSafeKey == null) {
            throw new BadRequestException("webSafeKey required");
        }

        Key<PlaceEntity> key = Key.create(webSafeKey);
        PlaceEntity entity = PlaceEntity.get(key);

        Collection<PlaceEntity> subEntities = entity.getSubPlaces();
        for (PlaceEntity subEntity : subEntities) {
            if (user != null) {
                subEntity.setRequestUser(user);
            }
            subEntity.setParent(entity);
        }

        return PlaceBean.fromEntities(subEntities);
    }

    @ApiMethod(name = "ratePlace", path = "rate/{webSafeKey}/{rating}", httpMethod = ApiMethod.HttpMethod.POST)
    public void ratePlace(User user, @Named("webSafeKey") String webSafeKey, @Named("rating") int rating)
            throws UnauthorizedException, BadRequestException {
        if (user == null) {
            throw new UnauthorizedException("Authentication required");
        }

        if (webSafeKey == null) {
            throw new BadRequestException("webSafeKey required");
        }

        if (rating == 0) {
            throw new BadRequestException("Nonzero rating required");
        }

        if (rating > 5) {
            throw new BadRequestException("Invalid rating > 5");
        }

        Key<PlaceEntity> key = Key.create(webSafeKey);
        PlaceEntity.ratePlace(user, key, rating);
    }

    private Index getPlaceIndex() {
        IndexSpec indexSpec = IndexSpec.newBuilder().setName("PLACE_INDEX").build();
        return SearchServiceFactory.getSearchService().getIndex(indexSpec);
    }

}