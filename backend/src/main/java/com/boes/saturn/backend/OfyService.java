package com.boes.saturn.backend;

import com.boes.saturn.backend.entity.PersonEntity;
import com.boes.saturn.backend.entity.PlaceEntity;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

public class OfyService {

    static {
        factory().register(PlaceEntity.class);
        factory().register(PersonEntity.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }

}