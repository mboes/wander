package com.boes.saturn.backend.bean;

import com.boes.saturn.backend.constant.Color;
import com.boes.saturn.backend.entity.PersonEntity;

import java.util.HashMap;
import java.util.Map;

public class PersonBean {

    public String webSafeKey;
    public String name;
    public Color favoriteColor;

    public static PersonBean fromEntity(PersonEntity entity) {
        PersonBean bean = new PersonBean();
        bean.webSafeKey = entity.getWebSafeKey();
        bean.name = entity.getName();
        bean.favoriteColor = entity.getFavoriteColor();
        return bean;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PersonBean)) {
            return false;
        }

        PersonBean that = (PersonBean) obj;
        return this.webSafeKey.equals(that.webSafeKey) &&
               this.name.equals(that.name) &&
               this.favoriteColor == that.favoriteColor;
    }

    @Override
    public String toString() {
        Map<String, Object> state = new HashMap<String, Object>();
        state.put("webSafeKey", webSafeKey);
        state.put("name", name);
        state.put("favoriteColor", favoriteColor);
        return state.toString();
    }

}
