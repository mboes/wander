package com.boes.saturn.backend.test;

import com.boes.saturn.backend.SaturnEndpoint;
import com.boes.saturn.backend.bean.PersonBean;
import com.boes.saturn.backend.bean.PlaceBean;
import com.boes.saturn.backend.constant.Color;
import com.boes.saturn.backend.entity.PersonEntity;
import com.boes.saturn.backend.entity.PlaceEntity;
import com.google.api.server.spi.response.BadRequestException;
import com.google.api.server.spi.response.NotFoundException;
import com.google.api.server.spi.response.UnauthorizedException;
import com.google.appengine.api.users.User;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.Key;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.boes.saturn.backend.OfyService.ofy;

public class SaturnEndpointTest {

    private LocalServiceTestHelper helper;
    private SaturnEndpoint endpoint;

    private void initDataStore(int unapplied) {
        if (helper != null) {
            helper.tearDown();
        }

        LocalDatastoreServiceTestConfig dataStoreConfig = new LocalDatastoreServiceTestConfig();

        if (unapplied == 0) {
            dataStoreConfig.setApplyAllHighRepJobPolicy();
        } else {
            dataStoreConfig.setDefaultHighRepJobPolicyUnappliedJobPercentage(unapplied);
        }

        helper = new LocalServiceTestHelper(dataStoreConfig);
        helper.setUp();
    }

    @Before
    public void setUp() {
        initDataStore(100);
        endpoint = new SaturnEndpoint();
    }

    @After
    public void tearDown() {
        ofy().clear();
    }

    @Test(expected = BadRequestException.class)
    public void failsToInsertPlaceWithoutName() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();

        endpoint.savePlace(user, bean);
    }

    @Test
    public void insertsPlace() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, bean).webSafeKey;

        PlaceBean inserted = endpoint.getPlace(null, webSafeKey);
        Assert.assertEquals("Wrong name", bean.name, inserted.name);
    }

    @Test
    public void updatesPlace() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, bean).webSafeKey;

        PlaceBean update = new PlaceBean();
        update.webSafeKey = webSafeKey;
        update.name = "Stanford University";

        endpoint.savePlace(user, update);

        PlaceBean stored = endpoint.getPlace(null, webSafeKey);
        Assert.assertEquals("Wrong name", update.name, stored.name);
        Assert.assertEquals("Wrong latitude", bean.latitude, stored.latitude, 0.00001);
        Assert.assertEquals("Wrong longitude", bean.longitude, stored.longitude, 0.00001);
    }

    @Test
    public void getsPlaceWithWebSafeKey() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, bean).webSafeKey;

        PlaceBean retrieved = endpoint.getPlace(null, webSafeKey);
        Assert.assertEquals("Wrong name", bean.name, retrieved.name);
    }

    @Test(expected = BadRequestException.class)
    public void failsToGetPlaceWithoutWebSafeKey() throws Exception {
        endpoint.getPlace(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsToGetPlaceWithBadWebSafeKey() throws Exception {
        endpoint.getPlace(null, "123");
    }

    @Test(expected = NotFoundException.class)
    public void failsToGetDeletedPlaceWithWebSafeKey() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, bean).webSafeKey;

        Key<PlaceEntity> key = Key.create(webSafeKey);
        ofy().delete().key(key).now();

        endpoint.getPlace(null, webSafeKey);
    }

    @Test(expected = NotFoundException.class)
    public void failsToUpdateDeletedPlace() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        PlaceBean update = endpoint.savePlace(user, bean);

        Key<PlaceEntity> key = Key.create(update.webSafeKey);
        ofy().delete().key(key).now();

        update.name = "Stanford University";
        endpoint.savePlace(user, update);
    }

    @Test(expected = BadRequestException.class)
    public void failsToGetPersonWithoutWebSafeKey() throws Exception {
        endpoint.getPerson(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsToGetPersonWithBadWebSafeKey() throws Exception {
        endpoint.getPerson("123");
    }

    @Test(expected = NotFoundException.class)
    public void failsToGetPersonWithoutUserAccount() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        String webSafeKey = PersonEntity.generateKey(user).getString();
        endpoint.getPerson(webSafeKey);
    }

    @Test(expected = UnauthorizedException.class)
    public void failsToSavePersonWithNullUserAccount() throws Exception {
        PersonBean bean = new PersonBean();
        endpoint.savePerson(null, bean);
    }

    @Test
    public void insertsAndGetsPersonWithDefaultName() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PersonBean bean = new PersonBean();

        String webSafeKey = endpoint.savePerson(user, bean).webSafeKey;

        PersonBean retrieved = endpoint.getPerson(webSafeKey);
        Assert.assertEquals("Wrong name", "boes.matt", retrieved.name);
        Assert.assertNotNull("No favorite color", retrieved.favoriteColor);
    }

    @Test
    public void insertsAndGetsPerson() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PersonBean bean = new PersonBean();
        bean.name = "Matt";
        bean.favoriteColor = Color.YELLOW;

        String webSafeKey = endpoint.savePerson(user, bean).webSafeKey;

        PersonBean retrieved = endpoint.getPerson(webSafeKey);
        Assert.assertEquals("Wrong name", "Matt", retrieved.name);
        Assert.assertEquals("Wrong color", Color.YELLOW, retrieved.favoriteColor);
    }

    @Test
    public void updatesAndGetsPerson() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        String webSafeKey = endpoint.savePerson(user, new PersonBean()).webSafeKey;

        PersonBean update = new PersonBean();
        update.name = "Matt";
        update.favoriteColor = Color.BLUE;
        endpoint.savePerson(user, update);

        PersonBean stored = endpoint.getPerson(webSafeKey);
        Assert.assertEquals("Wrong name", "Matt", stored.name);
        Assert.assertEquals("Wrong color", Color.BLUE, stored.favoriteColor);
    }

    @Test
    public void ignoresEmptyFieldsOnPartialPersonUpdate() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PersonBean bean = new PersonBean();
        bean.name = "Matt";
        String webSafeKey = endpoint.savePerson(user, bean).webSafeKey;

        PersonBean update = new PersonBean();
        update.name = "  ";
        endpoint.savePerson(user, update);

        PersonBean stored = endpoint.getPerson(webSafeKey);
        Assert.assertEquals("Wrong name", "Matt", stored.name);
        Assert.assertNotNull("No favorite color", stored.favoriteColor);
    }

    @Test(expected = BadRequestException.class)
    public void failsToSavePersonWithNullBean() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        endpoint.savePerson(user, null);
    }

    @Test(expected = BadRequestException.class)
    public void failsToSavePlaceWithNullBean() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        endpoint.savePlace(user, null);
    }

    @Test(expected = UnauthorizedException.class)
    public void failsToUpdatePlaceByDifferentUser() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        bean.webSafeKey = endpoint.savePlace(user, bean).webSafeKey;

        User hacker = new User("hacker@gmail.com", "gmail.com");
        endpoint.savePlace(hacker, bean);
    }

    @Test
    public void getsPlacesByPersonWithWebSafeKey() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford = endpoint.savePlace(user, stanford);

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        String webSafeKey = stanford.owner.webSafeKey;
        List<PlaceBean> beans = endpoint.getPlacesByPerson(webSafeKey);
        List<PlaceBean> expected = Arrays.asList(rodin, stanford);

        Assert.assertEquals("Wrong count", expected.size(), beans.size());
        Assert.assertTrue("Wrong places", beans.containsAll(expected));
        Assert.assertEquals("Wrong order", expected, beans);
    }

    @Test
    public void getsEmptyListOfPlacesByPerson() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        String webSafeKey = endpoint.savePerson(user, new PersonBean()).webSafeKey;
        List<PlaceBean> beans = endpoint.getPlacesByPerson(webSafeKey);
        Assert.assertTrue("Expected empty list", beans.isEmpty());
    }

    @Test(expected = NotFoundException.class)
    public void deletesPlaceByUser() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        bean = endpoint.savePlace(user, bean);
        endpoint.deletePlace(user, bean.webSafeKey);

        List<PlaceBean> beans = endpoint.getPlacesByPerson(bean.owner.webSafeKey);
        Assert.assertTrue("Expected empty list", beans.isEmpty());

        endpoint.getPlace(null, bean.webSafeKey);
    }

    @Test
    public void ignoresDeleteOnNonexistentPlace() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        bean = endpoint.savePlace(user, bean);
        endpoint.deletePlace(user, bean.webSafeKey);

        endpoint.deletePlace(user, bean.webSafeKey);
    }

    @Test(expected = UnauthorizedException.class)
    public void failsToDeletePlaceByDifferentUser() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean bean = new PlaceBean();
        bean.name = "Stanford";
        bean.latitude = 37.42991957045861;
        bean.longitude = -122.16942820698023;

        bean = endpoint.savePlace(user, bean);

        User hacker = new User("hacker@gmail.com", "gmail.com");
        endpoint.deletePlace(hacker, bean.webSafeKey);
    }

    @Test
    public void findsPlacesWithLimit() throws Exception {
        initDataStore(0);

        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford = endpoint.savePlace(user, stanford);

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        List<PlaceBean> beans = endpoint.findPlaces(1);
        Assert.assertEquals("Wrong number of places", 1, beans.size());

        List<PlaceBean> expected = Arrays.asList(stanford, rodin);
        beans = endpoint.findPlaces(100);

        Assert.assertEquals("Wrong number of places", expected.size(), beans.size());
        Assert.assertTrue("Wrong places", beans.containsAll(expected));
    }

    @Test
    public void insertsPlaceWithSubPlaces() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        PlaceBean dschool = new PlaceBean();
        dschool.name = "DSchool";
        dschool.latitude = 37.42609810319539;
        dschool.longitude = -122.17188913375138;
        dschool = endpoint.savePlace(user, dschool);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey, dschool.webSafeKey);
        stanford = endpoint.savePlace(user, stanford);

        Assert.assertEquals("Wrong name", "Stanford", stanford.name);
        Assert.assertEquals("Wrong owner name", "boes.matt", stanford.owner.name);
        Assert.assertEquals("Wrong sub count", 2, stanford.subCount);

        List<PlaceBean> expected = Arrays.asList(rodin, dschool);
        List<PlaceBean> subBeans = endpoint.getSubPlaces(null, stanford.webSafeKey);
        Assert.assertTrue("Wrong places", subBeans.equals(expected));
    }

    @Test
    public void getsPlaceWithNoSubPlaces() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;

        stanford = endpoint.savePlace(user, stanford);
        Assert.assertEquals("Wrong sub count", 0, stanford.subCount);

        List<PlaceBean> subBeans = endpoint.getSubPlaces(null, stanford.webSafeKey);
        Assert.assertEquals("Wrong number of places", 0, subBeans.size());
    }

    @Test
    public void updatesPlaceWithSubPlace() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey);
        stanford = endpoint.savePlace(user, stanford);

        PlaceBean dschool = new PlaceBean();
        dschool.name = "DSchool";
        dschool.latitude = 37.42609810319539;
        dschool.longitude = -122.17188913375138;
        dschool = endpoint.savePlace(user, dschool);

        stanford.subKeysToAdd = Arrays.asList(dschool.webSafeKey);
        stanford = endpoint.savePlace(user, stanford);

        List<PlaceBean> expected = Arrays.asList(rodin, dschool);
        Assert.assertEquals("Wrong sub count", expected.size(), stanford.subCount);

        List<PlaceBean> subBeans = endpoint.getSubPlaces(null, stanford.webSafeKey);
        Assert.assertTrue("Wrong places", subBeans.equals(expected));
    }

    @Test
    public void updatePlaceWithSubPlaceByDifferentUser() throws Exception {
        User matt = new User("boes.matt@gmail.com", "gmail.com");
        User jeff = new User("jeffjboes@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(matt, rodin);

        PlaceBean dschool = new PlaceBean();
        dschool.name = "DSchool";
        dschool.latitude = 37.42609810319539;
        dschool.longitude = -122.17188913375138;
        dschool = endpoint.savePlace(jeff, dschool);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey, dschool.webSafeKey);
        stanford = endpoint.savePlace(matt, stanford);

        List<PlaceBean> expected = Arrays.asList(rodin, dschool);
        Assert.assertEquals("Wrong sub count", expected.size(), stanford.subCount);

        List<PlaceBean> subBeans = endpoint.getSubPlaces(null, stanford.webSafeKey);
        Assert.assertTrue("Wrong places", subBeans.equals(expected));
    }

    @Test
    public void removesSubPlacesFromPlace() throws Exception {
        User matt = new User("boes.matt@gmail.com", "gmail.com");
        User jeff = new User("jeffjboes@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(jeff, rodin);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey);
        stanford = endpoint.savePlace(matt, stanford);

        Assert.assertEquals("Wrong sub count", 1, stanford.subCount);

        stanford.subKeysToRemove = Arrays.asList(rodin.webSafeKey);
        stanford = endpoint.savePlace(matt, stanford);
        Assert.assertEquals("Wrong sub count", 0, stanford.subCount);

        // Should already be removed, no consequence
        stanford.subKeysToRemove = Arrays.asList(rodin.webSafeKey);
        stanford = endpoint.savePlace(matt, stanford);
        Assert.assertEquals("Wrong sub count", 0, stanford.subCount);
    }

    @Test
    public void getPlaceWithDeletedSubPlace() throws Exception {
        User matt = new User("boes.matt@gmail.com", "gmail.com");
        User jeff = new User("jeffjboes@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(jeff, rodin);

        PlaceBean dschool = new PlaceBean();
        dschool.name = "DSchool";
        dschool.latitude = 37.42609810319539;
        dschool.longitude = -122.17188913375138;
        dschool = endpoint.savePlace(matt, dschool);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey, dschool.webSafeKey);
        stanford = endpoint.savePlace(matt, stanford);

        endpoint.deletePlace(jeff, rodin.webSafeKey);

        // The subCount for a place updates immediately after the first call to getSubPlaces for that place,
        // but not before it.  The slight lag in consistency is an acceptable trade-off.
        // See the same test below, after getSubPlaces is called.

        List<PlaceBean> expected = Arrays.asList(dschool, rodin);
        stanford = endpoint.getPlace(null, stanford.webSafeKey);
        Assert.assertEquals("Wrong sub count", expected.size(), stanford.subCount);

        expected = Arrays.asList(dschool);

        List<PlaceBean> subBeans = endpoint.getSubPlaces(null, stanford.webSafeKey);
        Assert.assertTrue("Wrong places", subBeans.equals(expected));

        stanford = endpoint.getPlace(null, stanford.webSafeKey);
        Assert.assertEquals("Wrong sub count", expected.size(), stanford.subCount);
    }

    @Test(expected = BadRequestException.class)
    public void failsToAddPlaceToItself() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        rodin.subKeysToAdd = Arrays.asList(rodin.webSafeKey);
        endpoint.savePlace(user, rodin);
    }

    @Test(expected = BadRequestException.class)
    public void failsToAddDuplicateSubPlace() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey);
        stanford = endpoint.savePlace(user, stanford);

        // Duplicate
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey);
        endpoint.savePlace(user, stanford);
    }

    @Test(expected = BadRequestException.class)
    public void failsToAddDuplicateSubPlaces() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        rodin = endpoint.savePlace(user, rodin);

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodin.webSafeKey, rodin.webSafeKey);  // Duplicates
        endpoint.savePlace(user, stanford);
    }

    @Test
    public void savePlaceWithDescription() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.description = "#1 university in U.S.";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, stanford).webSafeKey;

        PlaceBean bean = endpoint.getPlace(null, webSafeKey);
        Assert.assertEquals("Wrong description", stanford.description, bean.description);
    }

    @Test
    public void savePlaceWithTruncatedDescription() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;

        StringBuilder sb = new StringBuilder();
        while (sb.length() <= 140) {
            sb.append("a a");
        }
        stanford.description = sb.toString();
        Assert.assertTrue("Too short", stanford.description.length() > 140);

        String webSafeKey = endpoint.savePlace(user, stanford).webSafeKey;

        PlaceBean bean = endpoint.getPlace(null, webSafeKey);
        Assert.assertEquals("Wrong length", 140, bean.description.length());
    }

    @Test
    public void savePlaceWithEmptyDescription() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.description = "    ";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, stanford).webSafeKey;

        PlaceBean bean = endpoint.getPlace(null, webSafeKey);
        Assert.assertNull("Wrong description", bean.description);
    }

    @Test
    public void savePlaceWithLatLng() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;

        String webSafeKey = endpoint.savePlace(user, stanford).webSafeKey;

        PlaceBean bean = endpoint.getPlace(null, webSafeKey);

        Assert.assertEquals("Wrong latitude", stanford.latitude, bean.latitude, 0.00001);
        Assert.assertEquals("Wrong longitude", stanford.longitude, bean.longitude, 0.00001);
    }

    @Test(expected = BadRequestException.class)
    public void failsToSavePlaceWithNoLatLng() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        endpoint.savePlace(user, stanford);
    }

    @Test(expected = BadRequestException.class)
    public void failsToSavePlaceWithBadLatitude() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = -95;
        stanford.longitude = -122.16942820698023;
        endpoint.savePlace(user, stanford);
    }

    @Test(expected = BadRequestException.class)
    public void failsToSavePlaceWithBadLongitude() throws Exception {
        User user = new User("boes.matt@gmail.com", "gmail.com");
        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = 200.0;
        endpoint.savePlace(user, stanford);
    }

    @Test
    public void updatesPersonIgnoringEmailCapitalization() throws Exception {
        User MATT = new User("BOES.matt@GMAIL.COM", "gmail.COM");
        User matt = new User("boes.matt@gmail.com", "gmail.com");

        PersonBean bean;

        String bigWebSafeKey = endpoint.savePerson(MATT, new PersonBean()).webSafeKey;
        bean = endpoint.getPerson(bigWebSafeKey);
        Assert.assertEquals("Wrong name", "BOES.matt", bean.name);

        String littleWebSafeKey = endpoint.savePerson(matt, new PersonBean()).webSafeKey;
        bean = endpoint.getPerson(littleWebSafeKey);
        Assert.assertEquals("Wrong name", "BOES.matt", bean.name);

        PersonBean bigMatt = new PersonBean();
        bigMatt.name = "Matthew";
        endpoint.savePerson(MATT, bigMatt);
        PersonBean littleMatt = endpoint.getPerson(littleWebSafeKey);
        Assert.assertEquals("Wrong name", "Matthew", littleMatt.name);

        Assert.assertEquals("Different keys", littleWebSafeKey, bigWebSafeKey);
    }

    @Test
    public void ratesPlace() throws Exception {
        User creator = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        String webSafeKey = endpoint.savePlace(creator, stanford).webSafeKey;

        User jeff = new User("jeffjboes@gmail.com", "gmail.com");

        endpoint.ratePlace(jeff, webSafeKey, 3);
        stanford = endpoint.getPlace(jeff, webSafeKey);

        Assert.assertEquals("Wrong average rating", 3, stanford.averageRating);
        Assert.assertEquals("Wrong rating count", 1, stanford.ratingCount);
        Assert.assertEquals("Wrong user rating", 3, stanford.userRating);

        endpoint.ratePlace(jeff, webSafeKey, 5);
        stanford = endpoint.getPlace(jeff, webSafeKey);

        Assert.assertEquals("Wrong average rating", 5, stanford.averageRating);
        Assert.assertEquals("Wrong rating count", 1, stanford.ratingCount);
        Assert.assertEquals("Wrong user rating", 5, stanford.userRating);

        User lisa = new User("lndesimone@gmail.com", "gmail.com");

        endpoint.ratePlace(lisa, webSafeKey, 1);
        stanford = endpoint.getPlace(lisa, webSafeKey);

        Assert.assertEquals("Wrong average rating", 3, stanford.averageRating);
        Assert.assertEquals("Wrong rating count", 2, stanford.ratingCount);
        Assert.assertEquals("Wrong user rating", 1, stanford.userRating);

        stanford = endpoint.getPlace(jeff, webSafeKey);

        Assert.assertEquals("Wrong average rating", 3, stanford.averageRating);
        Assert.assertEquals("Wrong rating count", 2, stanford.ratingCount);
        Assert.assertEquals("Wrong user rating", 5, stanford.userRating);

        stanford = endpoint.getPlace(null, webSafeKey);

        Assert.assertEquals("Wrong average rating", 3, stanford.averageRating);
        Assert.assertEquals("Wrong rating count", 2, stanford.ratingCount);
        Assert.assertEquals("Wrong user rating", 0, stanford.userRating);
    }

//    @Test
//    public void getsPlaceAndItsParents() throws Exception {
//        initDataStore(0);
//
//        User matt = new User("boes.matt@gmail.com", "gmail.com");
//        User jeff = new User("jeffjboes@gmail.com", "gmail.com");
//        User lisa = new User("lndesimone@gmail.com", "gmail.com");
//
//        PlaceBean rodin = new PlaceBean();
//        rodin.name = "Rodin Garden";
//        rodin.latitude = 37.43236309856673;
//        rodin.longitude = -122.1709620952606;
//        String rodinKey = endpoint.savePlace(matt, rodin).webSafeKey;
//
//        rodin = endpoint.getPlace(matt, rodinKey);
//        Assert.assertTrue("Included in not empty", rodin.includedIn.isEmpty());
//
//        PlaceBean stanford = new PlaceBean();
//        stanford.name = "Stanford";
//        stanford.latitude = 37.42991957045861;
//        stanford.longitude = -122.16942820698023;
//        stanford.subKeysToAdd = Arrays.asList(rodinKey);
//        String stanfordKey = endpoint.savePlace(jeff, stanford).webSafeKey;
//
//        endpoint.ratePlace(lisa, stanfordKey, 3);
//        stanford = endpoint.getPlace(matt, stanfordKey);
//
//        PlaceBean cantorArt = new PlaceBean();
//        cantorArt.name = "Cantor Art";
//        cantorArt.latitude = 37.42609810319539;
//        cantorArt.longitude = -122.17188913375138;
//        cantorArt.subKeysToAdd = Arrays.asList(rodinKey);
//        String cantorKey = endpoint.savePlace(lisa, cantorArt).webSafeKey;
//
//        endpoint.ratePlace(jeff, cantorKey, 5);
//        cantorArt = endpoint.getPlace(matt, cantorKey);
//
//        rodin = endpoint.getPlace(matt, rodinKey);
//        List<PlaceBean> expected = Arrays.asList(cantorArt, stanford);
//        Assert.assertEquals("Wrong included in count", 2, rodin.includedIn.size());
//        Assert.assertTrue("Wrong included in", rodin.includedIn.containsAll(expected));
//        Assert.assertTrue("Wrong order of included in", rodin.includedIn.equals(expected));
//
//        List<PlaceBean> subBeans = endpoint.getSubPlaces(matt, stanfordKey);
//        Assert.assertEquals("Wrong number of sub places", 1, subBeans.size());
//        PlaceBean subBean = subBeans.get(0);
//        Assert.assertEquals("Wrong name", "Rodin Garden", subBean.name);
//
//        List<PlaceBean> otherParents = subBean.includedIn;
//        Assert.assertEquals("Wrong number of other parents", 1, otherParents.size());
//        PlaceBean otherParent = otherParents.get(0);
//        Assert.assertEquals("Wrong name", "Cantor Art", otherParent.name);
//
//        PlaceBean outdoorWalk = new PlaceBean();
//        outdoorWalk.name = "Outdoor walk";
//        outdoorWalk.latitude = 37.42609810319539;
//        outdoorWalk.longitude = -122.17188913375138;
//        outdoorWalk.subKeysToAdd = Arrays.asList(rodinKey);
//        endpoint.savePlace(lisa, outdoorWalk);
//
//        PlaceBean artScene = new PlaceBean();
//        artScene.name = "Art Scene";
//        artScene.latitude = 37.42609810319539;
//        artScene.longitude = -122.17188913375138;
//        artScene.subKeysToAdd = Arrays.asList(rodinKey);
//        String artSceneKey = endpoint.savePlace(lisa, artScene).webSafeKey;
//
//        endpoint.ratePlace(jeff, artSceneKey, 4);
//        artScene = endpoint.getPlace(matt, artSceneKey);
//
//        PlaceBean rodinSub = endpoint.getSubPlaces(matt, stanfordKey).get(0);
//        List<PlaceBean> expected2 = Arrays.asList(cantorArt, artScene);
//        List<PlaceBean> includedIn2 = rodinSub.includedIn;
//        Assert.assertEquals("Wrong included in count", 2, includedIn2.size());
//        Assert.assertTrue("Wrong included in", includedIn2.containsAll(expected2));
//        Assert.assertTrue("Wrong order of included in", includedIn2.equals(expected2));
//    }

    @Test
    public void getsSubPlacesWithUserRating() throws Exception {
        User matt = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        String rodinKey = endpoint.savePlace(matt, rodin).webSafeKey;

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodinKey);
        String stanfordKey = endpoint.savePlace(matt, stanford).webSafeKey;

        endpoint.ratePlace(matt, rodinKey, 4);

        List<PlaceBean> subBeans = endpoint.getSubPlaces(matt, stanfordKey);

        PlaceBean expected = endpoint.getPlace(matt, rodinKey);

        Assert.assertEquals("Wrong count", 1, subBeans.size());
        Assert.assertEquals("Wrong bean", expected, subBeans.get(0));
    }

    @Test
    public void getsPlaceAndSubPlaceContainingAnotherPlaceThatIsASubPlaceOfTheParent() throws Exception {
        initDataStore(0);

        User matt = new User("boes.matt@gmail.com", "gmail.com");

        PlaceBean rodin = new PlaceBean();
        rodin.name = "Rodin Garden";
        rodin.latitude = 37.43236309856673;
        rodin.longitude = -122.1709620952606;
        String rodinKey = endpoint.savePlace(matt, rodin).webSafeKey;

        PlaceBean stanford = new PlaceBean();
        stanford.name = "Stanford";
        stanford.latitude = 37.42991957045861;
        stanford.longitude = -122.16942820698023;
        stanford.subKeysToAdd = Arrays.asList(rodinKey);
        String stanfordKey = endpoint.savePlace(matt, stanford).webSafeKey;

        PlaceBean cantorArt = new PlaceBean();
        cantorArt.name = "Cantor Art";
        cantorArt.latitude = 37.42609810319539;
        cantorArt.longitude = -122.17188913375138;
        cantorArt.subKeysToAdd = Arrays.asList(rodinKey);
        String cantorKey = endpoint.savePlace(matt, cantorArt).webSafeKey;

        endpoint.getSubPlaces(matt, cantorKey);  // should FAIL because of circular reference from getIncludedIn?  Not sure
    }

}
